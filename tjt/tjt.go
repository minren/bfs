package tjt

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TmpJointest struct {
	Id           bson.ObjectId `json:"id" bson:"_id"`
	TestTracking string        `json:"testTracking" bson:"testTracking"`
	TestType     string        `json:"testType" bson:"testType"`
	Username     string        `json:"username" bson:"username"`
	JtSelect     []string      `json:"jtSelect" bson:"jtSelect"`
}

// TmpJointestManager manages a list of TmpJointests in memory.
type JtManager struct {
	g_session    *mgo.Session
	jts          []*TmpJointest
	mongoManager *mongo.MongoManager
}

// NewJtManager returns an empty JtManager.
func NewJtManager(session *mgo.Session) *JtManager {
	JtManager := &JtManager{}
	JtManager.g_session = session
	JtManager.mongoManager = mongo.NewMongoManager()
	return JtManager
}

// Save the given TmpJointest in the JtManager.
func (p *JtManager) Save(jts []TmpJointest) error {
	mylog.Info.Println("Save: %s", jts)
	// check if jt already exist

	var jtList []TmpJointest
	if err := p.GetAllTmpJointest(&jtList); err != nil {
		mylog.Error.Println("Save: GetAllTmpJointest failed %v", err)
		return nil
	}

	var actTmpJointest []TmpJointest
	var err error
	if len(jtList) == 0 {
		actTmpJointest = jts
	} else {
		for _, jt := range jts {
			dupTmpJointest := false
			for _, ejt := range jtList {
				if ejt.TestTracking == jt.TestTracking &&
					ejt.TestType == jt.TestType &&
					ejt.Username == jt.Username {
						mylog.Warning.Println("Save: jt already exist")
						dupTmpJointest = true
						break
					}
			}
			if dupTmpJointest == false {
				actTmpJointest = append(actTmpJointest, jt)
			}
		}
	}

	for _, jt := range actTmpJointest {
		//update database
		err = p.CreateTmpJointest(&jt)
		if err == nil {
			// add to list in memory
			_ = append(p.jts, &jt)
			mylog.Info.Println("Save: createTmpJointest succeed")
		} else {
			mylog.Error.Println("Save: createTmpJointest failed %v", err)
		}
	}

	return err
}

// All returns the list of all the TmpJointests in the JtManager.
func (p *JtManager) All(username string) ([]TmpJointest, error) {
	var jts []TmpJointest
	var validTmpJointests []TmpJointest
	err := p.GetAllTmpJointest(&jts)
	for _, jt := range jts {
		if jt.Username == username {
			validTmpJointests = append(validTmpJointests, jt)
		}
	}
	return validTmpJointests, err
}

func (p *JtManager) CreateTmpJointest(jt *TmpJointest) error {
	col := "jt"
	jt.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, jt)
	if err != nil {
		mylog.Error.Println("ERR: Insert jt failed")
		return err
	}
	mylog.Info.Println("CreateTmpJointest, successfully insert to mongo")

	return nil
}

func (p *JtManager) RemoveTmpJointest(jt *TmpJointest) error {
	col := "jt"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObjectById(col, jt.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove jt failed")
		return err
	}
	return nil
}


func (p *JtManager) RemoveUserAllTmpJointest(username string) error {
	col := "jt"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveAllObject(col, "username", username)
	if err != nil {
		mylog.Error.Println("ERR: remove jt failed")
		return err
	}
	return nil
}

func (p *JtManager) UpdateTmpJointest(jt *TmpJointest) error {
	col := "jt"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, jt, jt.Id)
}

func (p *JtManager) GetAllTmpJointest(jts *[]TmpJointest) error {
	col := "jt"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "_id", jts)
	if err != nil {
		mylog.Error.Println("ERR: query jts failed")
	}
	return err
}
