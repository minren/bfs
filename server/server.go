// It provides four methods:
//
// 	GET    /task/          Retrieves all the tasks.
// 	POST   /task/          Creates a new task given a title.
// 	GET    /task/{taskID}  Retrieves the task with the given id.
// 	PUT    /task/{taskID}  Updates the task with the given id.
//
// Every method below gives more information about every API call, its parameters, and its results.
package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/bfs/cm"
	"bitbucket.org/bfs/gene"
	"bitbucket.org/bfs/gv"
	"bitbucket.org/bfs/hplc"
	"bitbucket.org/bfs/library"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/op"
	"bitbucket.org/bfs/plateFormat"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/pv"
	"bitbucket.org/bfs/sequence"
	"bitbucket.org/bfs/sp"
	"bitbucket.org/bfs/user"
	"bitbucket.org/bfs/vp"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"time"
	"bitbucket.org/bfs/tjt"
)

type Server struct {
	session        *mgo.Session
	projectManager *project.ProjectManager
	geneManager    *gene.GeneManager
	libraryManager *library.LibraryManager
	opManager      *op.OpManager
	vpManager      *vp.VpManager
	spManager      *sp.SpManager
	pvManager      *pv.PvManager
	jtManager      *tjt.JtManager
	seqManager     *sequence.SequenceManager
	gvManager      *gv.GvManager
	pfManager      *plateFormat.PlateFormatManager
	cmManager      *cm.CmManager
	hplcManager    *hplc.HplcManager
	userManager    *user.UserManager
}

const ProjectPrefix = "/project/"
const GenePrefix = "/gene/"
const LibraryPrefix = "/library/"
const OpPrefix = "/op/"
const OpPrefixLib = "/op/{library}"
const VpPrefix = "/vp/"
const VpPrefixGene = "/vp/gene/{geneTracking}"
const VpPrefixTracking = "/vp/{tracking}"
const SpPrefix = "/sp/"
const PvPrefix = "/pv/"
const JtPrefix = "/jt"
const SeqPrefix = "/sequence"
const GvPrefix = "/gv/"
const PfPrefix = "/pf/"
const CMPrefix = "/colorimetric/"
const CMPrefixTracking = "/colorimetric/{tracking}"
const CMPrefixGene = "/colorimetric/gene/{geneTracking}"
const CMPrefixInvPc = "/colorimetric/invPc"
const CMPrefixInvVar = "/colorimetric/invVar"
const HplcPrefix = "/hplc/"
const HplcPrefixTracking = "/hplc/{tracking}"
const HplcPrefixGene = "/hplc/gene/{geneTracking}"
const UserPrefix = "/user/"
const UserPrefixUserName = "/user/{userName}"
const LoginPrefix = "/login/"
const LogoutPrefix = "/logout/"
const sessionTimeout = 60 * 60

var sessionStore *sessions.CookieStore

func setup() {
	// Note that both our authentication and encryption keys, respectively, are 32 bytes - as per
	// http://www.gorillatoolkit.org/pkg/sessions#NewCookieStore - we need a 32 byte enc. key for AES-256 encrypted cookies
	sessionStore = sessions.NewCookieStore([]byte("CAp1KsJncuMzARpetkqSFLqsBi5ag2bE"))
	sessionStore.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   86400,
		HttpOnly: true,
	}
	/*
		sessionStore.Options = &sessions.Options{
			Path: "/",
			Domain: "http://mydomain.com/",
			MaxAge: 3600 * 4,
			Secure: true,
			HttpOnly: true,
		}
	*/
}

type AuthenPair struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func NewServer(session *mgo.Session) *Server {
	server := &Server{}
	server.session = session
	server.projectManager = project.NewProjectManager(session)
	server.geneManager = gene.NewGeneManager(session)
	server.libraryManager = library.NewLibraryManager(session)
	server.opManager = op.NewOpManager(session)
	server.vpManager = vp.NewVpManager(session)
	server.spManager = sp.NewSpManager(session)
	server.pvManager = pv.NewPvManager(session)
	server.gvManager = gv.NewGvManager(session)
	server.pfManager = plateFormat.NewPlateFormatManager(session)
	server.cmManager = cm.NewCmManager(session)
	server.hplcManager = hplc.NewHplcManager(session)
	server.userManager = user.NewUserManager(session)
	server.seqManager = sequence.NewSequenceManager(session)
	server.jtManager = tjt.NewJtManager(session)
	setup()
	return server
}

func (s *Server) DbDataCleanup() {
	for {
		//clean up usersession
		if userSessions, err := s.userManager.GetAllUserSessions(); err == nil {
			for _, session := range userSessions {
				if time.Now().Sub(session.LastSeen).Seconds() > (sessionTimeout * 8) {
					//remove the session from userSession table
					if err := s.userManager.RemoveUserSession(&session); err != nil {
						mylog.Error.Println("remove user session err ", err)
					}
				}
			}
		}
		//clean up pv
		var pvs []pv.Pv
		if err := s.pvManager.GetAllPv(&pvs); err == nil {
			for _, pv := range pvs {
				if pv.Used == true {
					if err := s.pvManager.RemovePv(&pv); err != nil {
						mylog.Error.Println("Remove pv err", err)
					}
				}
			}
		}
		//clean up gv
		var gvs []gv.Gv
		if err := s.gvManager.GetAllGv(&gvs); err == nil {
			for _, gv := range gvs {
				if gv.Used == true {
					if err := s.gvManager.RemoveGv(&gv); err != nil {
						mylog.Error.Println("Remove gv err", err)
					}
				}
			}
		}
		time.Sleep(time.Hour * 8)
	}
}

func (s *Server) RegisterHandlers(port string, rootDir string) {
	r := mux.NewRouter()

	mylog.Info.Println("RegisterHandlers")
	r.HandleFunc(ProjectPrefix, errorHandler(s.addProject)).Methods("POST")
	r.HandleFunc(ProjectPrefix, errorHandler(s.listProjects)).Methods("Get")
	r.HandleFunc(GenePrefix, errorHandler(s.addGene)).Methods("POST")
	r.HandleFunc(GenePrefix, errorHandler(s.listGenes)).Methods("Get")
	r.HandleFunc(LibraryPrefix, errorHandler(s.addLibrary)).Methods("POST")
	r.HandleFunc(LibraryPrefix, errorHandler(s.listLibrarys)).Methods("Get")
	r.HandleFunc(OpPrefix, errorHandler(s.addOp)).Methods("POST")
	r.HandleFunc(OpPrefix, errorHandler(s.listOps)).Methods("Get")
	r.HandleFunc(OpPrefixLib, errorHandler(s.listOpsByLib)).Methods("Get")
	r.HandleFunc(VpPrefix, errorHandler(s.addVp)).Methods("POST")
	r.HandleFunc(VpPrefix, errorHandler(s.listVps)).Methods("Get")
	r.HandleFunc(VpPrefixGene, errorHandler(s.listVpByGeneTracking)).Methods("Get")
	r.HandleFunc(VpPrefixTracking, errorHandler(s.listVpByTracking)).Methods("Get")
	r.HandleFunc(SpPrefix, errorHandler(s.addSp)).Methods("POST")
	r.HandleFunc(SpPrefix, errorHandler(s.listSps)).Methods("Get")
	r.HandleFunc(PvPrefix, errorHandler(s.addPv)).Methods("POST")
	r.HandleFunc(PvPrefix, errorHandler(s.listPvs)).Methods("Get")
	r.HandleFunc(PvPrefix, errorHandler(s.updatePv)).Methods("PUT")
	r.HandleFunc(JtPrefix, errorHandler(s.listJts)).Methods("Get")
	r.HandleFunc(JtPrefix, errorHandler(s.addJt)).Methods("POST")
	r.HandleFunc(JtPrefix, errorHandler(s.deleteJts)).Methods("DELETE")
	r.HandleFunc(GvPrefix, errorHandler(s.addGv)).Methods("POST")
	r.HandleFunc(GvPrefix, errorHandler(s.listGvs)).Methods("Get")
	r.HandleFunc(GvPrefix, errorHandler(s.updateGv)).Methods("PUT")
	r.HandleFunc(PfPrefix, errorHandler(s.addPf)).Methods("POST")
	r.HandleFunc(PfPrefix, errorHandler(s.listPfs)).Methods("Get")
	r.HandleFunc(PfPrefix, errorHandler(s.updatePf)).Methods("PUT")
	r.HandleFunc(CMPrefix, errorHandler(s.addColorimetric)).Methods("POST")
	r.HandleFunc(CMPrefix, errorHandler(s.listColorimetric)).Methods("Get")
	r.HandleFunc(CMPrefixTracking, errorHandler(s.listColorimetricByTracking)).Methods("Get")
	r.HandleFunc(CMPrefixTracking, errorHandler(s.deleteCMByTracking)).Methods("DELETE")
	r.HandleFunc(CMPrefixGene, errorHandler(s.listColorimetricByGeneTracking)).Methods("Get")
	r.HandleFunc(CMPrefixInvPc, errorHandler(s.updateCM4pcInv)).Methods("POST")
	r.HandleFunc(CMPrefixInvVar, errorHandler(s.updateCM4VarInv)).Methods("POST")
	r.HandleFunc(HplcPrefix, errorHandler(s.addHplc)).Methods("POST")
	r.HandleFunc(HplcPrefix, errorHandler(s.listHplc)).Methods("Get")
	r.HandleFunc(HplcPrefixTracking, errorHandler(s.listHplcByTracking)).Methods("Get")
	r.HandleFunc(HplcPrefixGene, errorHandler(s.listHplcByGeneTracking)).Methods("Get")
	r.HandleFunc(UserPrefix, errorHandler(s.addUser)).Methods("POST")
	r.HandleFunc(UserPrefix, errorHandler(s.listUsers)).Methods("Get")
	r.HandleFunc(UserPrefixUserName, errorHandler(s.invalidUser)).Methods("DELETE")
	r.HandleFunc(UserPrefixUserName, errorHandler(s.updateUser)).Methods("PUT")
	r.HandleFunc(LoginPrefix, errorHandler(s.loginHandler)).Methods("POST")
	r.HandleFunc(LogoutPrefix, errorHandler(s.logoutHandler)).Methods("Get")
	r.HandleFunc(SeqPrefix, errorHandler(s.addSequence)).Methods("POST")
	r.HandleFunc(SeqPrefix, errorHandler(s.listSequences)).Methods("Get")

	http.Handle(ProjectPrefix, r)
	http.Handle(GenePrefix, r)
	http.Handle(LibraryPrefix, r)
	http.Handle(OpPrefix, r)
	http.Handle(VpPrefix, r)
	http.Handle(SpPrefix, r)
	http.Handle(PvPrefix, r)
	http.Handle(JtPrefix, r)
	http.Handle(SeqPrefix, r)
	http.Handle(GvPrefix, r)
	http.Handle(PfPrefix, r)
	http.Handle(CMPrefix, r)
	http.Handle(HplcPrefix, r)
	http.Handle(UserPrefix, r)
	http.Handle(LoginPrefix, r)
	http.Handle(LogoutPrefix, r)

	http.Handle("/", http.FileServer(http.Dir(rootDir)))
	http.ListenAndServe("0.0.0.0:"+port, nil)
}

// badRequest is handled by setting the status code in the reply to StatusBadRequest.
type badRequest struct{ error }

// notFound is handled by setting the status code in the reply to StatusNotFound.
type notFound struct{ error }

type internalError struct{ error }

// errorHandler wraps a function returning an error by handling the error and returning a http.Handler.
// If the error is of the one of the types defined above, it is handled as described for every type.
// If the error is of another type, it is considered as an internal error and its message is logged.
func errorHandler(f func(w http.ResponseWriter, r *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err == nil {
			return
		}
		switch err.(type) {
		case badRequest:
			http.Error(w, err.Error(), http.StatusBadRequest)
		case notFound:
			http.Error(w, "object not found", http.StatusNotFound)
		default:
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func (s *Server) logoutHandler(w http.ResponseWriter, r *http.Request) error {
	session, err := sessionStore.Get(r, "bfsQuantas")
	if err != nil {
		return internalError{err}
	}
	session.Values["userName"] = ""
	session.Values["sessionId"] = ""

	err = session.Save(r, w)
	if err != nil {
		return internalError{err}
	}

	// Re-direct to the dashboard
	http.Redirect(w, r, "/", 302)
	return nil
}

func (s *Server) loginHandler(w http.ResponseWriter, r *http.Request) error {
	session, err := sessionStore.Get(r, "bfsQuantas")
	if err != nil {
		return internalError{err}
	}

	var ErrCredentialsIncorrect = errors.New("Username and/or password incorrect.")
	var req AuthenPair
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	username := req.Username
	password := req.Password

	var oneUser user.User
	if err := s.userManager.GetOneUser(username, &oneUser); err != nil {
		// Save error in session flash
		session.AddFlash(ErrCredentialsIncorrect, "_errors")
		err := session.Save(r, w)
		if err != nil {
			return internalError{err}
		}
		http.Redirect(w, r, "login", 302)
		return badRequest{err}
	}

	// Leverage the bcrypt package's secure comparison.
	err = bcrypt.CompareHashAndPassword([]byte(oneUser.PasswordHash), []byte(password))
	if err != nil {
		// Save error in session flash
		session.AddFlash(ErrCredentialsIncorrect, "_errors")
		err := session.Save(r, w)
		if err != nil {
			return internalError{err}


		}
		http.Redirect(w, r, "login", 302)
		return badRequest{err}
	}

	uSession := user.UserSession{
		UserName: username,
	}
	sessionId, errs := s.userManager.CreateSession(&uSession)
	if errs != nil {
		return internalError{err}
	}
	session.Values["userName"] = oneUser.UserName
	session.Values["sessionId"] = sessionId

	err = session.Save(r, w)
	if err != nil {
		return internalError{err}
	}

	return json.NewEncoder(w).Encode(oneUser)
}

func (s *Server) sessionValidate(r *http.Request) (string, error) {
	session, err := sessionStore.Get(r, "bfsQuantas")
	if err != nil {
		return "", err
	}
	if sessionId, ok := session.Values["sessionId"]; ok {
		sessionID := sessionId.(string)
		var userSession user.UserSession
		if err := s.userManager.GetOneUserSessionById(sessionID, &userSession); err != nil {
			return "", err
		}
		sessionIdle := time.Now().Sub(userSession.LastSeen).Seconds()

		if sessionIdle >= sessionTimeout {
			return "", err
		}

		userSession.LastSeen = time.Now()
		if err := s.userManager.UpdateUserSession(&userSession); err != nil {
			return "", err
		}
		if userName, ok := session.Values["userName"]; ok {
			return userName.(string), nil
		}
		return "", errors.New("Invalid userName")
	} else {
		return "", errors.New("Invalid session id")
	}
}

func (s *Server) addProject(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addProject")
	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return errors.New("Not admin")
	}
	var req project.Project
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	dberr := s.projectManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("projectManager.Save failed!")
	}
	return nil
}

func inArray(d string, s []string) bool {
	for _, str := range s {
		if str == d {
			return true
		}
	}
	return false
}

func inProjectArray(projectTracking string, projects []project.Project) bool {
	for _, project := range projects {
		if projectTracking == project.Tracking {
			return true
		}
	}
	return false
}

func (s *Server) sessionAndUserValidate(r *http.Request) (error, bool, []project.Project, string) {
	var userName string
	var err error
	admin := false
	userName, err = s.sessionValidate(r)
	if err != nil {
		mylog.Error.Println("sessionValidate failed ", err)
		return err, false, nil, userName
	}
	var user user.User
	if err := s.userManager.GetOneUser(userName, &user); err != nil {
		mylog.Error.Println("get user failed")
		return err, false, nil, userName
	}
	var projects []project.Project
	res, _ := s.projectManager.All()

	if user.Role == "admin" {
		admin = true
		projects = res
	} else {
		for _, project := range res {
			if inArray(project.Tracking, user.Projects) {
				projects = append(projects, project)
			}
		}
	}
	return nil, admin, projects, userName
}

func (s *Server) listProjects(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	return json.NewEncoder(w).Encode(projects)
}

func (s *Server) addPf(w http.ResponseWriter, r *http.Request) error {
	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return errors.New("Not admin")
	}
	var req plateFormat.Pf
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	dberr := s.pfManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("plateFormatManager.Save failed!")
	}
	return nil
}

func (s *Server) listPfs(w http.ResponseWriter, r *http.Request) error {
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}

	res, _ := s.pfManager.All()
	return json.NewEncoder(w).Encode(res)
}

func (s *Server) updatePf(w http.ResponseWriter, r *http.Request) error {
	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return errors.New("Not admin")
	}

	mylog.Info.Println("Calling updatePf")
	var req plateFormat.Pf
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	if err := s.pfManager.UpdatePf(&req); err != nil {
		mylog.Error.Println("plateFormatManager.Update failed!")
		return badRequest{err}
	}
	return nil
}

func (s *Server) addPv(w http.ResponseWriter, r *http.Request) error {
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req []pv.Pv
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	for i := range req {
		req[i].Username = username
	}
	dberr := s.pvManager.Save(req)
	if dberr != nil {
		mylog.Error.Println("projectManager.Save failed!")
	}
	return nil
}

func (s *Server) updatePv(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling updatePv")
	if _, err := s.sessionValidate(r); err != nil {
		return err
	}
	var req []pv.Pv
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	for _, spv := range req {
		if err := s.pvManager.UpdatePv(&spv); err != nil {
			mylog.Error.Println("projectManager.Save failed!")
		}
	}

	return nil
}

func (s *Server) listPvs(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling listPvs")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	res, _ := s.pvManager.All(username)
	return json.NewEncoder(w).Encode(res)
}

func getBool(values map[string][]string, key string) (b bool, err error) {
	if vals, ok := values[key]; ok {
		b, err = strconv.ParseBool(vals[0])
		return b, err
	}
	return false, nil
}

func (s *Server) addSequence(w http.ResponseWriter, r *http.Request) error {
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req []sequence.Sequence
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	var overwrite bool
	var e error
	if overwrite, e = getBool(r.URL.Query(), "overwrite"); e != nil {
		return e
	}
	dberr := s.seqManager.Save(req, overwrite)
	if dberr != nil {
		mylog.Error.Println("sequenceManager.Save failed!")
		return dberr
	}
	return nil
}

func (s *Server) listSequences(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling listSequences")
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	res, _ := s.seqManager.All()
	return json.NewEncoder(w).Encode(res)
}

func (s *Server) addGv(w http.ResponseWriter, r *http.Request) error {
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req []gv.Gv
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db

	for i := range req {
		req[i].Username = username
	}
	mylog.Debug.Println(req)

	dberr := s.gvManager.Save(req)
	if dberr != nil {
		mylog.Error.Println("add gv failed!")
	}
	return nil
}

func (s *Server) updateGv(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling updateGv")
	if _, err := s.sessionValidate(r); err != nil {
		return err
	}
	var req []gv.Gv
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	for _, sgv := range req {
		if err := s.gvManager.UpdateGv(&sgv); err != nil {
			mylog.Error.Println("projectManager.Save failed!")
		}
	}

	return nil
}

func (s *Server) listGvs(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling listGvs")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	res, _ := s.gvManager.All(username)
	return json.NewEncoder(w).Encode(res)
}

func (s *Server) addGene(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addGene")
	if _, err := s.sessionValidate(r); err != nil {
		return err
	}
	var req gene.Gene
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	mylog.Debug.Println(req)
	// call project module and save the data to db
	dberr := s.geneManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("geneManager.Save failed!")
	}
	return nil
}

func (s *Server) listGenes(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var genes []gene.Gene
	res, _ := s.geneManager.All()
	for _, gene := range res {
		if inProjectArray(gene.Project.Tracking, projects) {
			genes = append(genes, gene)
		}
	}
	return json.NewEncoder(w).Encode(genes)
}

func (s *Server) addColorimetric(w http.ResponseWriter, r *http.Request) error {
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req cm.CMRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	req.Creator = username
	mylog.Debug.Println(req)
	// call project module and save the data to db
	dberr := s.cmManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("geneManager.Save failed!")
	}
	return nil
}

func (s *Server) listColorimetric(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var cms []cm.Colorimetric
	res, _ := s.cmManager.All()
	for _, cm := range res {
		if inProjectArray(cm.Project.Tracking, projects) {
			cms = append(cms, cm)
		}
	}
	return json.NewEncoder(w).Encode(cms)
}

func (s *Server) listColorimetricByTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	testTracking := vars["tracking"]
	var cms []cm.Colorimetric
	res, _ := s.cmManager.AllByTracking(testTracking)
	for _, cm := range res {
		if inProjectArray(cm.Project.Tracking, projects) {
			cms = append(cms, cm)
		}
	}
	return json.NewEncoder(w).Encode(cms)
}

func (s *Server) deleteCMByTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return json.NewEncoder(w).Encode(err)
	}
	vars := mux.Vars(r)
	testTracking := vars["tracking"]

	err1 := s.cmManager.RemoveCMsByTracking(testTracking)
	return json.NewEncoder(w).Encode(err1)
}

func (s *Server) listColorimetricByGeneTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	geneTracking := vars["geneTracking"]
	var cms []cm.Colorimetric
	res, _ := s.cmManager.AllByGeneTracking(geneTracking)
	for _, cm := range res {
		if inProjectArray(cm.Project.Tracking, projects) {
			cms = append(cms, cm)
		}
	}
	return json.NewEncoder(w).Encode(cms)
}

func (s *Server) updateCM4pcInv(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling invalidate pc")
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var cms []cm.Colorimetric
	if err := json.NewDecoder(r.Body).Decode(&cms); err != nil {
		return badRequest{err}
	}
	mylog.Debug.Println(cms)
	// call project module and save the data to db
	dberr := s.cmManager.InvPcUpdateCMs(cms)
	if dberr != nil {
		mylog.Error.Println("geneManager.Save failed!")
		return dberr
	}
	return nil
}

func (s *Server) updateCM4VarInv(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling invalidate variants")
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var cms []cm.Colorimetric
	if err := json.NewDecoder(r.Body).Decode(&cms); err != nil {
		return badRequest{err}
	}
	mylog.Debug.Println(cms)
	// call project module and save the data to db
	dberr := s.cmManager.InvalidCMsVariants(cms)
	if dberr != nil {
		mylog.Error.Println("cmManager delete failed!")
		return dberr
	}
	return nil
}

func (s *Server) addHplc(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addColorimetric")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req hplc.HplcRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	req.Creator = username
	mylog.Debug.Println(req)
	// call project module and save the data to db
	dberr := s.hplcManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("geneManager.Save failed!")
	}
	return nil
}

func (s *Server) listHplc(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var hplcs []hplc.Hplc
	res, _ := s.hplcManager.All()
	for _, hplc := range res {
		if inProjectArray(hplc.ProjectTracking, projects) {
			hplcs = append(hplcs, hplc)
		}
	}
	return json.NewEncoder(w).Encode(hplcs)
}

func (s *Server) listHplcByTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	tracking := vars["tracking"]
	var hplcs []hplc.Hplc
	res, _ := s.hplcManager.AllByTracking(tracking)
	for _, hplc := range res {
		if inProjectArray(hplc.ProjectTracking, projects) {
			hplcs = append(hplcs, hplc)
		}
	}
	return json.NewEncoder(w).Encode(hplcs)
}

func (s *Server) listHplcByGeneTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	tracking := vars["geneTracking"]
	var hplcs []hplc.Hplc
	res, _ := s.hplcManager.AllByGeneTracking(tracking)
	for _, hplc := range res {
		if inProjectArray(hplc.ProjectTracking, projects) {
			hplcs = append(hplcs, hplc)
		}
	}
	return json.NewEncoder(w).Encode(hplcs)
}

func (s *Server) addLibrary(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addLibrary")
	err, _, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req library.Library
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	dberr := s.libraryManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("libraryManager.Save failed!")
	}
	return nil
}

func (s *Server) listLibrarys(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var librarys []library.Library
	res, _ := s.libraryManager.All()
	for _, library := range res {
		if inProjectArray(library.Project.Tracking, projects) {
			librarys = append(librarys, library)
		}
	}
	return json.NewEncoder(w).Encode(librarys)
}

func (s *Server) addOp(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addOp")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req op.OpCreator
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	req.Creator = username
	// call project module and save the data to db
	mylog.Info.Println(req)
	dberr := s.opManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("opManager.Save failed!")
	}
	return nil
}

func (s *Server) listOps(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("opManager.listOps")
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var ops []op.Op
	res, _ := s.opManager.All()
	for _, op := range res {
		if inProjectArray(op.Project.Tracking, projects) {
			ops = append(ops, op)
		}
	}
	return json.NewEncoder(w).Encode(ops)
}

func (s *Server) listOpsByLib(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("opManager.listOpsByLib")
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	libraryTracking := vars["library"]
	var ops []op.Op
	res, _ := s.opManager.AllByLib(libraryTracking)
	for _, op := range res {
		if inProjectArray(op.Project.Tracking, projects) {
			ops = append(ops, op)
		}
	}
	return json.NewEncoder(w).Encode(ops)
}

func (s *Server) addVp(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addVp")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req vp.VpCreator
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	req.Creator = username
	mylog.Info.Println(req)
	dberr := s.vpManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("vpManager.Save failed!")
	}
	return nil
}

func (s *Server) listVps(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("vpManager.listVps")
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var vps []vp.Vp
	res, _ := s.vpManager.All()
	for _, vp := range res {
		if inProjectArray(vp.Project.Tracking, projects) {
			vps = append(vps, vp)
		}
	}
	return json.NewEncoder(w).Encode(vps)
}

func (s *Server) listVpByGeneTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	geneTracking := vars["geneTracking"]
	var vps []vp.Vp
	res, _ := s.vpManager.AllByGeneTracking(geneTracking)
	for _, vp := range res {
		if inProjectArray(vp.Project.Tracking, projects) {
			vps = append(vps, vp)
		}
	}
	return json.NewEncoder(w).Encode(vps)
}

func (s *Server) listVpByTracking(w http.ResponseWriter, r *http.Request) error {
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	vars := mux.Vars(r)
	tracking := vars["tracking"]
	var vps []vp.Vp
	res, _ := s.vpManager.AllByTracking(tracking)
	for _, vp := range res {
		if inProjectArray(vp.Project.Tracking, projects) {
			vps = append(vps, vp)
		}
	}
	return json.NewEncoder(w).Encode(vps)
}

func (s *Server) addSp(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addSp")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req sp.SpCreator
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	req.Creator = username
	mylog.Info.Println(req)
	dberr := s.spManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("spManager.Save failed!")
	}
	return nil
}

func (s *Server) listSps(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("spManager.listVps")
	err, _, projects, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var sps []sp.Sp
	res, _ := s.spManager.All()
	for _, sp := range res {
		if inProjectArray(sp.Project.Tracking, projects) {
			sps = append(sps, sp)
		}
	}
	return json.NewEncoder(w).Encode(sps)
}

// parseID obtains the id variable from the given request url,
// parses the obtained text and returns the result.
func parseID(r *http.Request) (int64, error) {
	txt, ok := mux.Vars(r)["id"]
	if !ok {
		return 0, fmt.Errorf("task id not found")
	}
	return strconv.ParseInt(txt, 10, 0)
}

func (s *Server) addUser(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling addUser")

	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}

	if !admin {
		return err
	}

	var req user.User
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	dberr := s.userManager.Save(&req)
	if dberr != nil {
		mylog.Error.Println("addUser failed!")
	}
	return nil
}

func (s *Server) listUsers(w http.ResponseWriter, r *http.Request) error {

	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return err
	}

	res, _ := s.userManager.All()
	return json.NewEncoder(w).Encode(res)
}

func (s *Server) updateUser(w http.ResponseWriter, r *http.Request) error {
	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return err
	}
	vars := mux.Vars(r)
	userName := vars["userName"]
	mylog.Info.Println("Calling updateUser", userName)
	var req user.User
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	if err := s.userManager.UpdateUser(&req); err != nil {
		mylog.Error.Println("updateUser failed!", err)
		return badRequest{err}
	}
	return nil
}

func (s *Server) invalidUser(w http.ResponseWriter, r *http.Request) error {
	err, admin, _, _ := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if !admin {
		return err
	}
	vars := mux.Vars(r)
	userName := vars["userName"]
	mylog.Info.Println("Calling deleteUser", userName)
	// call project module and save the data to db
	dberr := s.userManager.InvalidUser(userName)
	if dberr != nil {
		mylog.Error.Println("deleteUser failed!")
	}
	return nil
}

func (s *Server) addJt(w http.ResponseWriter, r *http.Request) error {
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	var req []tjt.TmpJointest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return badRequest{err}
	}
	// call project module and save the data to db
	mylog.Debug.Println(req)
	for i := range req {
		req[i].Username = username
	}
	dberr := s.jtManager.Save(req)
	if dberr != nil {
		mylog.Error.Println("jtManager.Save failed!")
	}
	return nil
}


func (s *Server) listJts(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling listPvs")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	res, _ := s.jtManager.All(username)
	return json.NewEncoder(w).Encode(res)
}


func (s *Server) deleteJts(w http.ResponseWriter, r *http.Request) error {
	mylog.Info.Println("Calling listPvs")
	err, _, _, username := s.sessionAndUserValidate(r)
	if err != nil {
		return err
	}
	if r.FormValue("all") == "yes" {
		s.jtManager.RemoveUserAllTmpJointest(username)
	} else {
		var req []tjt.TmpJointest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			return badRequest{err}
		}
		for _,jt := range req {
			s.jtManager.RemoveTmpJointest(&jt)
		}
	}
	return json.NewEncoder(w).Encode("")
}

