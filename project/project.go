package project

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/tracking"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

type Project struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	Tracking    string        `json:"tracking" bson:"tracking"`
	Type        string        `json:"type" bson:"type"`
	Description string        `json:"description" bson:"description"`
	Start       string        `json:"start" bson:"start"`
	Notes       string        `json:"notes" bson:"notes"`
	Date        string        `json:"date" bson:"date"`
}

func NewProject(tracking string, types string, description string, start string, notes string) (*Project, error) {
	var newP Project
	newP.Tracking = tracking
	newP.Type = types
	newP.Description = description
	newP.Start = start
	newP.Notes = notes
	return &newP, nil
}

// ProjectManager manages a list of projects in memory.
type ProjectManager struct {
	g_session    *mgo.Session
	projects     []*Project
	mongoManager *mongo.MongoManager
}

// NewProjectManager returns an empty ProjectManager.
func NewProjectManager(session *mgo.Session) *ProjectManager {
	projectManager := &ProjectManager{}
	projectManager.g_session = session
	projectManager.mongoManager = mongo.NewMongoManager()
	return projectManager
}

// Save the given Project in the ProjectManager.
func (p *ProjectManager) Save(project *Project) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", project)
	for _, t := range p.projects {
		if t.Tracking == project.Tracking {
			err := errors.New("Project tracking already exist")
			return err
		}
	}
	//update database
	err := p.CreateProject(project)
	if err == nil {
		// add to list in memory
		_ = append(p.projects, project)
		mylog.Info.Println("Save: createProject succeed")
	} else {
		mylog.Error.Println("Save: createProject failed %v", err)
	}

	return err
}

// All returns the list of all the Projects in the ProjectManager.
func (p *ProjectManager) All() ([]Project, error) {
	var projects []Project
	err := p.GetAllProject(&projects)
	return projects, err
}

// Find returns the Project with the given id in the ProjectManager
func (p *ProjectManager) One(tracking string) Project {
	if tracking == "" {
		return Project{}
	}
	for _, t := range p.projects {
		if t.Tracking == tracking {
			return *t
		}
	}

	project := new(Project)
	if err := p.GetOneProject(tracking, project); err == nil {
		p.projects = append(p.projects, project)
	}
	return *project
}

func (p *ProjectManager) CreateProject(project *Project) error {
	col := "project"
	project.Id = bson.NewObjectId()
	project.Date = time.Now().String()
	var trackNum string
	if _, err := tracking.ReserveTracking("project", &trackNum, 1); err != nil {
		return err
	}
	project.Tracking = trackNum
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, project)
	if err != nil {
		mylog.Error.Println("ERR: Insert project failed")
		return err
	}
	log.Println("CreateProject, successfully insert to mongo")
	// Read project back
	var readProject Project
	err1 := p.GetOneProject(project.Tracking, &readProject)
	if err1 != nil {
		mylog.Error.Println("CreateProject, read back failed")
	} else {
		mylog.Info.Println("CreateProject, successfully read back %s", readProject)
	}

	return nil
}

func (p *ProjectManager) RemoveProject(project *Project) error {
	col := "project"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", project.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: remove project failed")
		return err
	}
	return nil
}

func (p *ProjectManager) UpdateProject(project *Project) error {
	col := "project"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, project, "tracking", project.Tracking)
}

func (p *ProjectManager) GetOneProject(tracking string, project *Project) error {
	col := "project"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, project)
	if err != nil {
		mylog.Error.Println("ERR: query one project failed")
	}
	return err
}

func (p *ProjectManager) GetAllProject(projects *[]Project) error {
	col := "project"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "tracking", projects)
	if err != nil {
		mylog.Error.Println("ERR: query projects failed")
	}
	return err
}
