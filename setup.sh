#!/bin/bash
ifconfig eno50332208 172.16.207.2/24
docker ps | grep mongo
if [ $? -eq 0 ]; then
	docker rm mongo
fi
docker run -d --name mongo -p 27017:27017 mongodb
iptables -I INPUT 1 -i eno50332208 -p tcp --dport 8081 -j ACCEPT
