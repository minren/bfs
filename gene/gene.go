package gene

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/tracking"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Gene struct {
	Id         bson.ObjectId   `json:"id" bson:"_id"`
	Tracking   string          `json:"tracking" bson:"tracking"`
	Project    project.Project `json:"project" bson:"project"`
	Name       string          `json:"name" bson:"name"`
	Class      string          `json:"class" bson:"class"`
	Colony     string          `json:"colony" bson:"colony"`
	Accession  string          `json:"accession" bson:"accession"`
	Source     string          `json:"source" bson:"source"`
	Genotype   string          `json:"genotype" bson:"genotype"`
	Peptide    string          `json:"peptide" bson:"peptide"`
	Codon      string          `json:"codon" bson:"codon"`
	Tag        string          `json:"tag" bson:"tag"`
	Vector     string          `json:"vector" bson:"vector"`
	Cloning    string          `json:"cloning" bson:"cloning"`
	Expression string          `json:"expression" bson:"expression"`
	Notes      string          `json:"notes" bson:"notes"`
	Date       string          `json:"date" bson:"date"`
	Selected   bool            `json:"selected"`
}

// GeneManager manages a list of genes in memory.
type GeneManager struct {
	g_session    *mgo.Session
	genes        []*Gene
	mongoManager *mongo.MongoManager
}

// NewGeneManager returns an empty GeneManager.
func NewGeneManager(session *mgo.Session) *GeneManager {
	geneManager := &GeneManager{}
	geneManager.g_session = session
	geneManager.mongoManager = mongo.NewMongoManager()
	return geneManager
}

// Save the given Gene in the GeneManager.
func (p *GeneManager) Save(gene *Gene) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", gene)
	for _, t := range p.genes {
		if t.Tracking == gene.Tracking {
			err := errors.New("Gene tracking already exist")
			return err
		}
	}
	//update database
	err := p.CreateGene(gene)
	if err == nil {
		// add to list in memory
		_ = append(p.genes, gene)
		mylog.Info.Println("Save: createGene succeed")
	} else {
		mylog.Error.Println("Save: createGene failed %v", err)
	}

	return err
}

// All returns the list of all the Genes in the GeneManager.
func (p *GeneManager) All() ([]Gene, error) {
	var genes []Gene
	err := p.GetAllGene(&genes)
	return genes, err
}

// Find returns the Gene with the given id in the GeneManager
func (p *GeneManager) One(tracking string) Gene {
	if tracking == "" {
		return Gene{}
	}
	for _, t := range p.genes {
		if t.Tracking == tracking {
			return *t
		}
	}

	gene := new(Gene)
	if err := p.GetOneGene(tracking, gene); err == nil {
		p.genes = append(p.genes, gene)
	}
	return *gene
}

func (p *GeneManager) CreateGene(gene *Gene) error {
	col := "gene"
	gene.Id = bson.NewObjectId()
	gene.Date = time.Now().String()
	var trackNum string

	if _, err := tracking.ReserveTracking("gene", &trackNum, 1); err != nil {
		mylog.Error.Println("CreateGene: ReserveTracking failed!")
		return err
	}
	gene.Tracking = trackNum

	if gene.Colony == "" {
		if _, err := tracking.ReserveTracking("colony", &(gene.Colony), 1); err != nil {
			mylog.Error.Println("CreateGene: ReserveTracking for colony failed!")
			return err
		}
	}
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, gene)
	if err != nil {
		mylog.Error.Println("ERR: Insert gene failed")
		return err
	}
	mylog.Info.Println("CreateGene, successfully insert to mongo")
	// Read gene back
	var readGene Gene
	err1 := p.GetOneGene(gene.Tracking, &readGene)
	if err1 != nil {
		mylog.Error.Println("CreateGene, read back failed")
	} else {
		mylog.Info.Println("CreateGene, successfully read back %s", readGene)
	}

	return nil
}

func (p *GeneManager) RemoveGene(gene *Gene) error {
	col := "gene"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", gene.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: Insert gene failed")
		return err
	}
	return nil
}

func (p *GeneManager) UpdateGene(gene *Gene) error {
	col := "gene"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, gene, "tracking", gene.Tracking)
}

func (p *GeneManager) GetOneGene(tracking string, gene *Gene) error {
	col := "gene"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, gene)
	if err != nil {
		mylog.Error.Println("ERR: query one gene failed")
	}
	return err
}

func (p *GeneManager) GetAllGene(genes *[]Gene) error {
	col := "gene"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "tracking", genes)
	if err != nil {
		mylog.Error.Println("ERR: query genes failed")
	}
	return err
}
