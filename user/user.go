package user

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

type User struct {
	Id           bson.ObjectId `json:"id" bson:"_id"`
	UserName     string        `json:"userName" bson:"userName"`
	FirstName    string        `json:"firstName" bson:"firstName"`
	LastName     string        `json:"lastName" bson:"lastName"`
	Email        string        `json:"email" bson:"email"`
	Password     string        `json:"password"`
	PasswordHash string        `bson:"passwordHash"`
	Role         string        `json:"role" bson:"role"`
	Projects     []string      `json:"projects" bson:"projects"`
	Selected     bool          `json:"selected"`
	Invalid      bool          `bson:"invalid"`
}

type UserSession struct {
	Id        bson.ObjectId `bson:"_id"`
	UserName  string        `bson:"userName"`
	LoginTime time.Time     `bson:"loginTime"`
	LastSeen  time.Time     `bson:"lastSeen"`
}

// UserManager manages a list of users in memory.
type UserManager struct {
	g_session    *mgo.Session
	users        []*User
	mongoManager *mongo.MongoManager
}

// NewUserManager returns an empty UserManager.
func NewUserManager(session *mgo.Session) *UserManager {
	userManager := &UserManager{}
	userManager.g_session = session
	userManager.mongoManager = mongo.NewMongoManager()
	return userManager
}

// Save the given User in the UserManager.
func (p *UserManager) Save(user *User) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", user)

	//update database
	err := p.CreateUser(user)
	if err == nil {
		mylog.Info.Println("Save: createUser succeed")
	} else {
		mylog.Error.Println("Save: createUser failed %v", err)
	}

	return err
}

// All returns the list of all the Users in the UserManager.
func (p *UserManager) All() ([]User, error) {
	var users []User
	err := p.GetAllUser(&users)
	var validUsers []User
	for _, user := range users {
		if user.Invalid == false {
			validUsers = append(validUsers, user)
		}
	}
	return validUsers, err
}

func (p *UserManager) CreateSession(session *UserSession) (string, error) {
	col := "userSession"
	session.Id = bson.NewObjectId()
	session.LoginTime = time.Now()
	session.LastSeen = time.Now()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, session)
	if err != nil {
		mylog.Error.Println("ERR: Insert gene failed")
		return "", err
	}
	return string(session.Id), nil
}

func (p *UserManager) GetOneUserSessionById(sessionId string, session *UserSession) error {
	col := "userSession"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObjectById(col, bson.ObjectId(sessionId), session)
	if err != nil {
		mylog.Error.Println("ERR: query one user failed")
	}
	return err
}

func (p *UserManager) UpdateUserSession(session *UserSession) error {
	col := "userSession"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, session, session.Id)
}

func (p *UserManager) GetAllUserSessions() ([]UserSession, error) {
	col := "userSession"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	var session []UserSession
	err := p.mongoManager.GetAllObject(col, "_id", &session)
	if err != nil {
		mylog.Error.Println("ERR: query users failed")
		return nil, err
	}
	return session, err
}

func (p *UserManager) RemoveUserSession(session *UserSession) error {
	col := "userSession"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	fmt.Println("removing")
	err := p.mongoManager.RemoveObjectById(col, session.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove user session failed", err)
		return err
	}
	fmt.Println("removing done")
	return nil
}

func (p *UserManager) CreateUser(user *User) error {
	col := "user"
	user.Id = bson.NewObjectId()

	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	var err error
	var cryptedPassword []byte
	cryptedPassword, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		mylog.Error.Println("create hashed password failed")
		return errors.New("create hashed password failed")
	}
	user.Password = ""
	user.PasswordHash = string(cryptedPassword)
	user.Invalid = false
	err = p.mongoManager.CreateObject(col, user)
	if err != nil {
		mylog.Error.Println("ERR: Insert user failed")
		return err
	}
	log.Println("CreateUser, successfully insert to mongo")
	var readUser User
	// Read user back
	err2 := p.GetOneUser(user.UserName, &readUser)
	if err2 != nil {
		mylog.Error.Println("CreateUser, read back failed")
	} else {
		mylog.Info.Println("CreateUser, successfully read back %s", readUser)
	}

	return nil
}

func (p *UserManager) RemoveUser(userName string) error {
	col := "user"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "userName", userName)
	if err != nil {
		mylog.Error.Println("ERR: remove user failed", err)
		return err
	}
	return nil
}

func (p *UserManager) InvalidUser(userName string) error {
	var user User
	if err := p.GetOneUser(userName, &user); err != nil {
		return err
	}
	user.Invalid = true
	if err := p.UpdateUser(&user); err != nil {
		return err
	}
	return nil
}

func (p *UserManager) UpdateUser(user *User) error {
	col := "user"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, user, user.Id)
}

func (p *UserManager) GetOneUser(userName string, user *User) error {
	col := "user"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "userName", userName, user)
	if err != nil {
		mylog.Error.Println("ERR: query one user failed")
	}
	return err
}

func (p *UserManager) GetAllUser(users *[]User) error {
	col := "user"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "userName", users)
	if err != nil {
		mylog.Error.Println("ERR: query users failed")
	}
	return err
}
