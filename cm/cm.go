package cm

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/op"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/sp"
	"bitbucket.org/bfs/tracking"
	"bitbucket.org/bfs/vp"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"math"
	"strconv"
	"strings"
	"time"
)

type PcResult struct {
	pcAvg  float64
	pcStd  float64
	pcCv   float64
	pf     float64
	pcAvgn float64
	pcStdn float64
	pcCvn  float64
	pfn    float64
}

type Result struct {
	Plate    string `json:"plate"`
	Index    int    `json:"index"`
	Test     string `json:"test"`
	Wellcode string `json:"wellcode"`
}

type CMRequest struct {
	Name        string   `json:"name"`
	Reactionph  string   `json:"reactionph"`
	Temperature string   `json:"temperature"`
	Time        string   `json:"time"`
	Dilution    string   `json:"dilution"`
	Notes       string   `json:"notes"`
	Type        string   `json:"type"`
	PlateType   string   `json:"plateType"`
	Results     []Result `json:"results"`
	Creator     string
}

type Colorimetric struct {
	Id              bson.ObjectId   `json:"id" bson:"_id"`
	Tracking        string          `json:"tracking" bson:"tracking"`
	PlateTracking   string          `json:"plateTracking" bson:"plateTracking"`
	Project         project.Project `json:"project" bson:"project"`
	LibraryName     string          `json:"libraryName" bson:"libraryName"`
	LibraryTracking string          `json:"libraryTracking" bson:"libraryTracking"`
	GeneTracking    string          `json:"geneTracking" bson:"geneTracking"`
	Name            string          `json:"name"`
	Reactionph      string          `json:"reactionph"`
	Temperature     string          `json:"temperature"`
	Time            string          `json:"time"`
	Dilution        string          `json:"dilution"`
	Notes           string          `json:"notes"`
	Type            string          `json:"type"`
	PlateType       string          `json:"plateType"`
	PlateFormat     string          `json:"plateFormat" bson:"plateFormat"`
	WellDefinition  string          `json:"wellDefinition" bson:"wellDefinition"`
	WellCode        string          `json:"wellCode" bson:"wellCode"`
	Colony          string          `json:"colony" bson:"colony"`
	DataType        string          `json:"dataType" bson:"dataType"`
	Creator         string          `json:"creator" bson:"creator"`
	Date            string          `json:"date" bson:"date"`
	Od              string          `json:"od" bson:"od"`
	Pcavg           string          `json:"pcavg" bson:"pcavg"`
	Pcstd           string          `json:"pcstd" bson:"pcstd"`
	Pccv            string          `json:"pccv" bson:"pccv"`
	Pf              string          `json:"pf" bson:"pf"`
	Odn             string          `json:"odn" bson:"odn"`
	Pcavgn          string          `json:"pcavgn" bson:"pcavgn"`
	Pcstdn          string          `json:"pcstdn" bson:"pcstdn"`
	Pccvn           string          `json:"pccvn" bson:"pccvn"`
	Pfn             string          `json:"pfn" bson:"pfn"`
	PcInvalid       bool            `json:"pcInvalid" bson:"pcInvalid"`
	Selected        bool            `json:"selected"`
	VaInvalid       bool            `json:"vaInvalid" bson:"vaInvalid"`
}

// cmManager manages a list of colorimetrics in memory.
type CmManager struct {
	g_session     *mgo.Session
	colorimetrics []*Colorimetric
	mongoManager  *mongo.MongoManager
	opManager     *op.OpManager
	vpManager     *vp.VpManager
	spManager     *sp.SpManager
}

// NewGeneManager returns an empty cmManager.
func NewCmManager(session *mgo.Session) *CmManager {
	cmManager := &CmManager{}
	cmManager.g_session = session
	cmManager.mongoManager = mongo.NewMongoManager()
	cmManager.opManager = op.NewOpManager(session)
	cmManager.vpManager = vp.NewVpManager(session)
	cmManager.spManager = sp.NewSpManager(session)

	cmManager.mongoManager.CopySession(session)
	defer cmManager.mongoManager.CloseSession()

	cmManager.mongoManager.BuildIndex("colorimetric", []string{"plateTracking", "tracking", "geneTracking"})
	return cmManager
}

// All returns the list of all the Ops in the OpManager.
func (p *CmManager) All() ([]Colorimetric, error) {
	var cms []Colorimetric
	err := p.GetAllCMs(&cms)
	return cms, err
}

func (p *CmManager) AllByTracking(tracking string) ([]Colorimetric, error) {
	var cms []Colorimetric
	err := p.GetAllCMsByTracking(tracking, &cms)
	return cms, err
}

func (p *CmManager) AllByGeneTracking(tracking string) ([]Colorimetric, error) {
	var cms []Colorimetric
	err := p.GetAllCMsByGeneTracking(tracking, &cms)
	return cms, err
}

func (p *CmManager) InvalidCMsVariants(cms []Colorimetric) error {
	for _,colm := range cms {
		p.InvalidCM(colm)
	}
	return nil
}

func (p *CmManager) InvPcUpdateCMs(pcs []Colorimetric) error {
	//convert to map
	invPcMap := make(map[string][]Colorimetric)
	for _, pc := range pcs {
		if pcs, ok := invPcMap[pc.PlateTracking]; ok {
			invPcMap[pc.PlateTracking] = append(pcs, pc)
		} else {
			invPcMap[pc.PlateTracking] = append([]Colorimetric{}, pc)
		}
	}
	//update db plate by plate

	for plateTracking, invPCs := range invPcMap {
		var plateCms []Colorimetric
		var cms []Colorimetric
		// read cms from db
		if err := p.GetAllCMsByPlate(plateTracking, &plateCms); err != nil {
			return err
		}
		for _, cm := range plateCms {
			if cm.Tracking == invPCs[0].Tracking {
				cms = append(cms, cm)
			}
		}
		//update PcInvalid field
		for _, invPc := range invPCs {
			for ind, cm := range cms {
				if cm.DataType == "Positive Control" && invPc.WellDefinition == cm.WellDefinition {
					cms[ind].PcInvalid = true
				}
			}
		}

		validPcVal := []float64{}
		rmbPcVal := []float64{}
		ncVal := []float64{}

		for _, validCm := range cms {
			if validCm.DataType == "Positive Control" && validCm.PcInvalid == false {
				vod, _ := strconv.ParseFloat(validCm.Od, 64)
				validPcVal = append(validPcVal, vod)
			} else if validCm.DataType == "Negative Control" {
				vod, _ := strconv.ParseFloat(validCm.Od, 64)
				ncVal = append(ncVal, vod)
			}
		}

		avgPc := 0.0
		avgNc := 0.0
		pcStd := 0.0
		pcCv := 0.0
		fmt.Println("valid pc ", validPcVal)
		fmt.Println("valid pc length", len(validPcVal))
		fmt.Println("cm length", len(cms))

		if len(validPcVal) != 0 {
			avgPc = calAvg(validPcVal)
			pcStd = calStd(validPcVal)
			pcCv = calCv(avgPc, pcStd)
		}
		if len(ncVal) != 0 {
			avgNc = calAvg(ncVal)
		}

		for _, pcv := range validPcVal {
			rmbPcVal = append(rmbPcVal, pcv-avgNc)
		}

		for index, cm := range cms {
			cms[index].Pcavg = strconv.FormatFloat(avgPc, 'f', 6, 64)
			cms[index].Pcstd = strconv.FormatFloat(pcStd, 'f', 6, 64)
			cms[index].Pccv = strconv.FormatFloat(pcCv, 'f', 6, 64)
			cms[index].Pcavgn = strconv.FormatFloat(calAvg(rmbPcVal), 'f', 6, 64)
			cms[index].Pcstdn = strconv.FormatFloat(calStd(rmbPcVal), 'f', 6, 64)
			cms[index].Pccvn = strconv.FormatFloat(calCv(calAvg(rmbPcVal), calStd(rmbPcVal)), 'f', 6, 64)

			od, _ := strconv.ParseFloat(cm.Od, 64)
			if len(validPcVal) == 0 && avgNc != 0.0 {
				pf := od / avgNc
				cms[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
				cms[index].Pfn = cms[index].Pf
			} else {
				pf := od / avgPc
				cms[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
				pfn := (od - avgNc) / calAvg(rmbPcVal)
				cms[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
			}
		}
		//update cms to db
		if err := p.UpdateAllCMsByPlate(plateTracking, cms); err != nil {
			return err
		}
	}
	return nil
}

func (p *CmManager) UpdateAllCMsByPlate(plateTracking string, cms []Colorimetric) error {
	col := "colorimetric"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	for _, cm := range cms {
		err := p.mongoManager.UpdateObjectById(col, cm, cm.Id)
		if err != nil {
			mylog.Error.Println("ERR: update colorimetrics failed", cm.Id, cm.PlateTracking, cm.WellDefinition, err)
			return err
		}
	}
	return nil
}

func (p *CmManager) GetAllCMsByPlate(plateTracking string, cms *[]Colorimetric) error {
	col := "colorimetric"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "plateTracking", plateTracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query colorimetrics failed", plateTracking)
	}
	return err
}

func (p *CmManager) GetAllCMsByTracking(tracking string, cms *[]Colorimetric) error {
	col := "colorimetric"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", tracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query colorimetrics failed", tracking)
	}
	return err
}

func (p *CmManager) GetAllCMsByGeneTracking(tracking string, cms *[]Colorimetric) error {
	col := "colorimetric"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "geneTracking", tracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query colorimetrics failed", tracking)
	}
	return err
}

func (p *CmManager) RemoveCMsByTracking(tracking string) error {
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	col := "colorimetric"
	err := p.mongoManager.RemoveAllObject(col, "tracking", tracking)
	if err != nil {
		mylog.Error.Println("ERR: remove cm failed")
		return err
	}
	return nil
}


func (p *CmManager) RemoveCM(cm Colorimetric) error {
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	col := "colorimetric"
	err := p.mongoManager.RemoveObjectById(col, cm.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove cm failed")
		return err
	}
	return nil
}


func (p *CmManager) InvalidCM(cm Colorimetric) error {
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	col := "colorimetric"
	cm.VaInvalid = true
	err := p.mongoManager.UpdateObjectById(col, cm, cm.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove cm failed")
		return err
	}
	return nil
}


func (p *CmManager) GetAllCMs(cms *[]Colorimetric) error {
	col := "colorimetric"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetAllObject(col, "_id", cms)
	if err != nil {
		mylog.Error.Println("ERR: query all colorimetric failed")
	}
	return err
}

func (p *CmManager) GetOneCM(tracking string, cm *Colorimetric) error {
	col := "colorimetric"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, cm)
	if err != nil {
		mylog.Error.Println("ERR: query one colorimetric failed")
	}
	return err
}

// Save the given Colorimetric in the cmManager.
func (p *CmManager) Save(request *CMRequest) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", request)
	//update database
	if colorimetrics, err := p.GenerateCM(request); err == nil {
		for _, colorimetric := range colorimetrics {
			//update database
			//log.Println("--each op", opi)
			err := p.CreateCM(colorimetric)
			if err == nil {
				// add to list in memory
				_ = append(p.colorimetrics, colorimetric)
				mylog.Info.Println("Save: createOp succeed")
			} else {
				mylog.Error.Println("Save: createOp failed %v", err)
				return err
			}
		}
		return nil
	} else {
		mylog.Error.Println("Save: generate op data failed!")
		return err
	}
}

func (p *CmManager) CreateCM(colorimetric *Colorimetric) error {
	col := "colorimetric"
	colorimetric.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, colorimetric)
	if err != nil {
		mylog.Error.Println("ERR: Insert colorimetric failed")
		return err
	}
	mylog.Info.Println("CreateCM, successfully insert to mongo")
	// Read op back
	var readCM Colorimetric
	err1 := p.GetOneCM(colorimetric.Tracking, &readCM)
	if err1 != nil {
		mylog.Error.Println("CreateCM, read back failed")
	} else {
		mylog.Info.Println("CreateCM, successfully read back %s", readCM)
	}

	return nil
}

func (p *CmManager) GenerateCM(request *CMRequest) ([]*Colorimetric, error) {

	var trackNum string

	if _, err := tracking.ReserveTracking("test", &trackNum, 1); err != nil {
		mylog.Error.Println("Create Colorimetric: ReserveTracking failed!")
		return nil, err
	}
	results := request.Results

	plateMap := make(map[string]interface{})
	pcMap := make(map[string][]float64)
	ncMap := make(map[string][]float64)

	var colorimetrics []*Colorimetric

	preTracking := ""
	for _, result := range results {
		if result.Plate != preTracking {
			if request.PlateType == "op" {
				var ops []op.Op
				if err := p.opManager.GetAllOpByTracking(result.Plate, &ops); err == nil {
					if len(ops) != 0 {
						opMap := make(map[string]interface{})
						opWellMap := make(map[string]interface{})
						for _,op := range ops {
							opWellMap[op.WellDefinition] = op
						}
						opMap["wellMap"] = opWellMap
						plateMap[result.Plate] = opMap
					}
				}
			} else if request.PlateType == "vp" {
				var vps []vp.Vp
				if err := p.vpManager.GetAllVpByTracking(result.Plate, &vps); err == nil {
					if len(vps) != 0 {
						vpMap := make(map[string]interface{})
						vpWellMap := make(map[string]interface{})
						for _,vp := range vps {
							vpWellMap[vp.WellDefinition] = vp
						}
						vpMap["wellMap"] = vpWellMap
						plateMap[result.Plate] = vpMap
					}
				}
			} else if request.PlateType == "sp" {
				var sps []sp.Sp
				if err := p.spManager.GetAllSpByTracking(result.Plate, &sps); err == nil {
					if len(sps) != 0 {
						spMap := make(map[string]interface{})
						spWellMap := make(map[string]interface{})
						for _,sp := range sps {
							spWellMap[sp.WellDefinition] = sp
						}
						spMap["wellMap"] = spWellMap
						plateMap[result.Plate] = spMap
					}
				}
			} else {
				mylog.Error.Println("Wrong plate type ", request.PlateType)
			}
			preTracking = result.Plate
		}
	}

	var pcss []float64
	var ncs []float64
	var pMap map[string]interface{}
	var wellMap map[string]interface{}
	pcss = nil
	ncs = nil

	preTracking = ""

	for _, result := range results {
		colorimetric := new(Colorimetric)
		colorimetric.Date = time.Now().String()
		colorimetric.Tracking = trackNum
		colorimetric.Reactionph = request.Reactionph
		colorimetric.Temperature = request.Temperature
		colorimetric.Time = request.Time
		colorimetric.Name = request.Name
		colorimetric.Type = request.Type
		colorimetric.Dilution = request.Dilution
		colorimetric.Notes = request.Notes
		colorimetric.PcInvalid = false
		colorimetric.Creator = request.Creator
		plateTracking := result.Plate

		if preTracking != plateTracking {
			pMapIntf := plateMap[plateTracking]
			var ok2 bool
			pMap,ok2 = pMapIntf.(map[string]interface{})
			if !ok2 {
				mylog.Error.Println("convert plateMap error")
			}
			wellMap = pMap["wellMap"].(map[string]interface{})
			if len(pcss) != 0 && preTracking != "" {
				pcMap[preTracking] = pcss
				ncMap[preTracking] = ncs
				pcss = nil
				ncs = nil
			}
			preTracking = plateTracking

			//build well-definition map
		}

		result.Test = strings.TrimSuffix(result.Test, "\r")
		testResult, _ := strconv.ParseFloat(result.Test, 64)

		plateType, ok := wellMap[result.Wellcode]
		if !ok {
			continue
		}

		switch plate := plateType.(type) {
		case op.Op:
			colorimetric.Colony = plate.Colony
			colorimetric.PlateTracking = plateTracking
			colorimetric.LibraryName = plate.Library.Name
			colorimetric.LibraryTracking = plate.Library.Tracking
			colorimetric.GeneTracking = plate.Library.Gene.Tracking
			colorimetric.WellDefinition = plate.WellDefinition
			colorimetric.WellCode = plate.WellCode
			colorimetric.Project = plate.Project
			colorimetric.DataType = plate.DataType
			colorimetric.PlateFormat = plate.Format
			colorimetric.PlateType = plate.Type
			colorimetric.Od = strconv.FormatFloat(testResult, 'f', 6, 64)
			if plate.DataType == "Positive Control" {
				pcss = append(pcss, testResult)
			} else if plate.DataType == "Negative Control" {
				ncs = append(ncs, testResult)
			}
		case vp.Vp:
			colorimetric.Colony = plate.Colony
			colorimetric.PlateTracking = plateTracking
			colorimetric.LibraryName = plate.LibraryName
			colorimetric.LibraryTracking = plate.LibraryTracking
			colorimetric.GeneTracking = plate.Gene.Tracking
			colorimetric.WellDefinition = plate.WellDefinition
			colorimetric.WellCode = plate.WellCode
			colorimetric.Project = plate.Project
			colorimetric.DataType = plate.DataType
			colorimetric.PlateFormat = plate.Format
			colorimetric.PlateType = plate.Type
			colorimetric.Od = strconv.FormatFloat(testResult, 'f', 6, 64)
			if plate.DataType == "Positive Control" {
				pcss = append(pcss, testResult)
			} else if plate.DataType == "Negative Control" {
				ncs = append(ncs, testResult)
			}
		case sp.Sp:
			colorimetric.Colony = plate.Colony
			colorimetric.PlateTracking = plateTracking
			colorimetric.LibraryName = plate.LibraryName
			colorimetric.LibraryTracking = plate.LibraryTracking
			colorimetric.GeneTracking = plate.Gene.Tracking
			colorimetric.WellDefinition = plate.WellDefinition
			colorimetric.WellCode = plate.WellCode
			colorimetric.Project = plate.Project
			colorimetric.DataType = plate.DataType
			colorimetric.PlateFormat = plate.Format
			colorimetric.PlateType = plate.Type
			colorimetric.Od = strconv.FormatFloat(testResult, 'f', 6, 64)
			if plate.DataType == "Positive Control" {
				pcss = append(pcss, testResult)
			} else if plate.DataType == "Negative Control" {
				ncs = append(ncs, testResult)
			}
		default:
			mylog.Error.Println("colorimetric get wrong type")
		}
		colorimetrics = append(colorimetrics, colorimetric)
	}

	if len(pcss) != 0 {
		pcMap[results[len(results)-1].Plate] = pcss
	}
	if len(ncs) != 0 {
		ncMap[results[len(results)-1].Plate] = ncs
	}

	// get ncAvg for each plate
	ncAvgMap := make(map[string]float64)
	for plate, nc := range ncMap {
		if len(nc) != 0 {
			ncAvgMap[plate] = calAvg(nc)
		}
	}

	PcResultMap := make(map[string]PcResult)

	for plate, pcs := range pcMap {
		var pcRes PcResult
		pcRes.pcAvg = calAvg(pcs)
		pcRes.pcStd = calStd(pcs)
		pcRes.pcCv = calCv(pcRes.pcAvg, pcRes.pcStd)

		ncAvg := ncAvgMap[plate]
		for ind, platePc := range pcs {
			pcs[ind] = platePc - ncAvg
		}
		pcRes.pcAvgn = calAvg(pcs)
		pcRes.pcStdn = calStd(pcs)
		pcRes.pcCvn = calCv(pcRes.pcAvgn, pcRes.pcStdn)

		PcResultMap[plate] = pcRes
	}

	for index, colorimetric := range colorimetrics {
		var pcavgn float64
		if len(pcss) != 0 {
			pcavg := PcResultMap[colorimetric.PlateTracking].pcAvg
			colorimetrics[index].Pcavg = strconv.FormatFloat(pcavg, 'f', 6, 64)
			pcstd := PcResultMap[colorimetric.PlateTracking].pcStd
			colorimetrics[index].Pcstd = strconv.FormatFloat(pcstd, 'f', 6, 64)
			pccv := PcResultMap[colorimetric.PlateTracking].pcCv
			colorimetrics[index].Pccv = strconv.FormatFloat(pccv, 'f', 6, 64)

			pcavgn = PcResultMap[colorimetric.PlateTracking].pcAvgn
			colorimetrics[index].Pcavgn = strconv.FormatFloat(pcavgn, 'f', 6, 64)
			pcstdn := PcResultMap[colorimetric.PlateTracking].pcStdn
			colorimetrics[index].Pcstdn = strconv.FormatFloat(pcstdn, 'f', 6, 64)
			pccvn := PcResultMap[colorimetric.PlateTracking].pcCvn
			colorimetrics[index].Pccvn = strconv.FormatFloat(pccvn, 'f', 6, 64)
		}
		ncAvg := ncAvgMap[colorimetric.PlateTracking]

		od, _ := strconv.ParseFloat(colorimetric.Od, 64)
		colorimetric.Odn = strconv.FormatFloat((od - ncAvg), 'f', 6, 64)
		if len(pcss) == 0 && ncAvg != 0 {
			pf := od / ncAvg
			colorimetrics[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
			pfn := od / ncAvg
			colorimetrics[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
		} else if len(pcss) != 0 {
			pf := od / PcResultMap[colorimetric.PlateTracking].pcAvg
			colorimetrics[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
			pfn := (od - ncAvg) / pcavgn
			colorimetrics[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
		}
	}

	return colorimetrics, nil
}

func calAvg(dataSet []float64) float64 {
	var sum float64
	sum = 0.0
	if len(dataSet) == 0 {
		return 0.0
	}
	for _, data := range dataSet {
		sum += data
	}
	dlength := len(dataSet)
	if dlength == 0 {
		return 0
	}
	avg := sum / float64(dlength)
	return avg
}

func calStd(dataSet []float64) float64 {
	avg := calAvg(dataSet)
	sum := 0.0
	for _, data := range dataSet {
		sum = sum + math.Abs(data-avg)*math.Abs(data-avg)
	}
	return math.Sqrt(sum / float64(len(dataSet)))
}

func calCv(avg float64, std float64) float64 {
	return std * 100 / avg
}
