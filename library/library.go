package library

import (
	"bitbucket.org/bfs/gene"
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/tracking"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Library struct {
	Id       bson.ObjectId   `json:"id" bson:"_id"`
	Tracking string          `json:"tracking" bson:"tracking"`
	Project  project.Project `json:"project" bson:"project"`
	Gene     gene.Gene       `json:"gene" bson:"gene"`
	Name     string          `json:"name" bson:"name"`
	Type     string          `json:"type" bson:"type"`
	Design   string          `json:"design" bson:"design"`
	Notes    string          `json:"notes" bson:"notes"`
	Date     string          `json:"date" bson:"date"`
}

// LibraryManager manages a list of librarys in memory.
type LibraryManager struct {
	g_session    *mgo.Session
	librarys     []*Library
	mongoManager *mongo.MongoManager
}

// NewLibraryManager returns an empty LibraryManager.
func NewLibraryManager(session *mgo.Session) *LibraryManager {
	libraryManager := &LibraryManager{}
	libraryManager.g_session = session
	libraryManager.mongoManager = mongo.NewMongoManager()
	return libraryManager
}

// Save the given Library in the LibraryManager.
func (p *LibraryManager) Save(library *Library) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", library)
	for _, t := range p.librarys {
		if t.Tracking == library.Tracking {
			err := errors.New("Library tracking already exist")
			return err
		}
	}
	//update database
	err := p.CreateLibrary(library)
	if err == nil {
		// add to list in memory
		_ = append(p.librarys, library)
		mylog.Info.Println("Save: createLibrary succeed")
	} else {
		mylog.Error.Println("Save: createLibrary failed %v", err)
	}

	return err
}

// All returns the list of all the Librarys in the LibraryManager.
func (p *LibraryManager) All() ([]Library, error) {
	var librarys []Library
	err := p.GetAllLibrary(&librarys)
	return librarys, err
}

// Find returns the Library with the given id in the LibraryManager
func (p *LibraryManager) One(tracking string) Library {
	if tracking == "" {
		return Library{}
	}
	for _, t := range p.librarys {
		if t.Tracking == tracking {
			return *t
		}
	}

	library := new(Library)
	if err := p.GetOneLibrary(tracking, library); err == nil {
		p.librarys = append(p.librarys, library)
	}
	return *library
}

func (p *LibraryManager) CreateLibrary(library *Library) error {
	col := "library"
	library.Id = bson.NewObjectId()
	library.Date = time.Now().String()
	var trackNum string
	if _, err := tracking.ReserveTracking("library", &trackNum, 1); err != nil {
		return err
	}
	library.Tracking = trackNum
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, library)
	if err != nil {
		mylog.Error.Println("ERR: Insert library failed")
		return err
	}
	mylog.Info.Println("CreateLibrary, successfully insert to mongo")
	// Read library back
	var readLibrary Library
	err1 := p.GetOneLibrary(library.Tracking, &readLibrary)
	if err1 != nil {
		mylog.Error.Println("CreateLibrary, read back failed")
	} else {
		mylog.Info.Println("CreateLibrary, successfully read back %s", readLibrary)
	}

	return nil
}

func (p *LibraryManager) RemoveLibrary(library *Library) error {
	col := "library"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", library.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: Insert library failed")
		return err
	}
	return nil
}

func (p *LibraryManager) UpdateLibrary(library *Library) error {
	col := "library"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, library, "tracking", library.Tracking)
}

func (p *LibraryManager) GetOneLibrary(tracking string, library *Library) error {
	col := "library"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, library)
	if err != nil {
		mylog.Error.Println("ERR: query one library failed")
	}
	return err
}

func (p *LibraryManager) GetAllLibrary(librarys *[]Library) error {
	col := "library"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "tracking", librarys)
	if err != nil {
		mylog.Error.Println("ERR: query librarys failed")
	}
	return err
}
