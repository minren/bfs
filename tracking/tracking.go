package tracking

import (
	"bitbucket.org/bfs/mylog"
	"encoding/json"
	"errors"
	"log"
	"os"
	"strconv"
)


var TrackingFile string


type Tracking struct {
	OriginalPlate   int `json:"originalPlate"`
	ValidationPlate int `json:"validationPlate"`
	SequencePlate   int `json:"sequencePlate"`
	Project         int `json:"project"`
	Library         int `json:"library"`
	Gene            int `json:"gene"`
	Colony          int `json:"colony"`
	Test            int `json:"test"`
}

/*
project = PC_FBS_001
library = L000001
gene = G000001
colony = CL00000001
test = T000001
originalPlate = OP000001
validationPlate = VP000001
sequencePlate = SP000001
*/

func getTrackingValue(tracking *Tracking, trackingType string) int {
	switch trackingType {
	case "project":
		return tracking.Project
	case "library":
		return tracking.Library
	case "gene":
		return tracking.Gene
	case "colony":
		return tracking.Colony
	case "test":
		return tracking.Test
	case "originalPlate":
		return tracking.OriginalPlate
	case "validationPlate":
		return tracking.ValidationPlate
	case "sequencePlate":
		return tracking.SequencePlate
	default:
		log.Println("getTrackingValue: get wrong type %s", trackingType)
		return 0
	}
}

func setTrackingValue(tracking *Tracking, trackingType string, value int) {
	switch trackingType {
	case "project":
		tracking.Project = value
		break
	case "gene":
		tracking.Gene = value
		break
	case "library":
		tracking.Library = value
		break
	case "colony":
		tracking.Colony = value
		break
	case "test":
		tracking.Test = value
		break
	case "originalPlate":
		tracking.OriginalPlate = value
		break
	case "validationPlate":
		tracking.ValidationPlate = value
		break
	case "sequencePlate":
		tracking.SequencePlate = value
		break
	default:
		mylog.Error.Println("setTrackingValue: get wrong type %s", trackingType)
		return
	}
}

func ConvertInt2String(t int, trackingType string) string {
	typeLen := map[string]int{
		"project":         3,
		"library":         6,
		"gene":            6,
		"test":            6,
		"originalPlate":   6,
		"validationPlate": 6,
		"sequencePlate":   6,
		"colony":          8,
	}

	var ts string
	if trackingLen, ok := typeLen[trackingType]; ok {
		ts = strconv.Itoa(t)
		l := trackingLen - len(ts)
		a := ""
		for i := 0; i < l; i++ {
			a = a + "0"
		}
		ts = a + ts
	} else {
		log.Println("ConvertInt2String: wrong trackingType %s", trackingType)
		return ""
	}

	switch trackingType {
	case "project":
		return "PC_INT_" + ts
	case "library":
		return "IL" + ts
	case "gene":
		return "IG" + ts
	case "colony":
		return "ICL" + ts
	case "test":
		return "IT" + ts
	case "originalPlate":
		return "IOP" + ts
	case "validationPlate":
		return "IVP" + ts
	case "sequencePlate":
		return "ISP" + ts
	default:
		log.Println("ConvertInt2String: get wrong type %s", trackingType)
	}
	return ""
}

func ReserveTracking(trackingType string, trackingNumber *string, number int) ([]string, error) {
	if listTrackingNumber, err := readTracking(trackingType, trackingNumber, number); err == nil {
		if err := updateTracking(trackingType, number); err != nil {
			return nil, err
		}
		return listTrackingNumber, nil
	} else {
		return nil, err
	}
}

func updateTracking(trackingType string, num int) error {
	tracking := new(Tracking)
	err := readTrackingFile(tracking)
	if err != nil {
		mylog.Error.Println("Read tracking number failed")
		return err
	}

	trackingNumber := getTrackingValue(tracking, trackingType)
	if trackingNumber != 0 {
		setTrackingValue(tracking, trackingType, trackingNumber+num)
	} else {
		mylog.Error.Println("PickTracking: Wrong trackingType", tracking)
		return errors.New("Wrong trackingType")
	}

	if err := writeTrackingFile(*tracking); err != nil {
		mylog.Error.Println("writeTrackingFile failed")
		return err
	}

	return nil
}

func readTracking(trackingType string, trackingNumber *string, num int) ([]string, error) {
	tracking := new(Tracking)
	err := readTrackingFile(tracking)
	if err != nil {
		mylog.Error.Println("Read tracking number failed")
		return nil, err
	}

	number := getTrackingValue(tracking, trackingType)
	if number != 0 {
		*trackingNumber = ConvertInt2String(number, trackingType)
		listTrackingNum := []string{}
		for i := 0; i < num; i++ {
			tmp := ConvertInt2String(number+i, trackingType)
			listTrackingNum = append(listTrackingNum, tmp)
		}
		return listTrackingNum, nil
	} else {
		mylog.Error.Println("ReadTracking: Wrong trackingType", tracking)
		return nil, errors.New("Wrong trackingType")
	}
}

func readTrackingFile(tracking *Tracking) error {
	if _, err := os.Stat(TrackingFile); os.IsNotExist(err) {
		initialTracking := Tracking{
			OriginalPlate:   1,
			ValidationPlate: 1,
			SequencePlate:   1,
			Project:         1,
			Library:         1,
			Gene:            1,
			Colony:          1,
			Test:            1,
		}

		log.Println("no such file and write initial value: %s", TrackingFile)
		if err1 := writeTrackingFile(initialTracking); err1 != nil {
			mylog.Error.Println("Write the initial tracking number error")
			return err1
		}
		*tracking = initialTracking
		return nil
	}

	if trackFile, err2 := os.Open(TrackingFile); err2 == nil {
		jsonParser := json.NewDecoder(trackFile)
		if err3 := jsonParser.Decode(tracking); err3 != nil {
			log.Println("Decode the tracking file error")
			return err3
		} else {
			return nil
		}
	} else {
		mylog.Error.Println("open tracking file failed")
		return err2
	}
}

func writeTrackingFile(tracking Tracking) error {
	trackFile, err := os.Create(TrackingFile)
	if err == nil {
		defer trackFile.Close()

		jsonParser := json.NewEncoder(trackFile)
		if err1 := jsonParser.Encode(tracking); err1 != nil {
			mylog.Error.Println("Write tracking file error")
			return err1
		}
		return nil
	}
	return errors.New("create tracking file failed")
}
