package sp

import (
	"time"
	"strings"

	"bitbucket.org/bfs/gene"
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/op"
	"bitbucket.org/bfs/plateFormat"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/pv"
	"bitbucket.org/bfs/tracking"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Sp struct {
	Id              bson.ObjectId   `json:"id" bson:"_id"`
	Tracking        string          `json:"tracking" bson:"tracking"`
	Project         project.Project `json:"project" bson:"project"`
	Gene            gene.Gene       `json:"gene" bson:"gene"`
	LibraryTracking string          `json:"libraryTracking" bson:"libraryTracking"`
	LibraryName     string          `json:"libraryName" bson:"libraryName"`
	Type            string          `json:"type" bson:"type"`
	Format          string          `json:"format" bson:"format"`
	WellDefinition  string          `json:"wellDefinition" bson:"wellDefinition"`
	WellCode        string          `json:"wellCode" bson:"wellCode"`
	Colony          string          `json:"colony" bson:"colony"`
	DataType        string          `json:"dataType" bson:"dataType"`
	Creator         string          `json:"creator" bson:"creator"`
	Date            string          `json:"date" bson:"date"`
	Result          interface{}     `json:"result" bson:"result"`
	Others          interface{}     `json:"others" bson:"others"`
	Notes           string          `json:"notes" bson:"notes"`
	OpTracking      string          `json:"opTracking" bson:"opTracking"`
	OpWell          string          `json:"opWell" bson:"opWell"`
	Amount          int             `json:"amount" bson:"amount"`
	Source          string          `json:"source" bson:"source"`
}

type SpCreator struct {
	Pc      []gene.Gene    `json:"pc" bson:"pc"`
	Nc      []gene.Gene    `json:"nc" bson:"nc"`
	Amount  int            `json:"amount" bson:"amount"`
	Notes   string         `json:"notes" bson:"notes"`
	Layout  plateFormat.Pf `json:"layout" bson:"layout"`
	Source  string         `json:"source" bson:"source"`
	List    string         `json:"list" bson:"list"`
	Creator string
}

// SpManager manages a list of Sps in memory.
type SpManager struct {
	g_session    *mgo.Session
	sps          []*Sp
	pvManager    *pv.PvManager
	opManager    *op.OpManager
	mongoManager *mongo.MongoManager
	pfManager    *plateFormat.PlateFormatManager
}

// NewSpManager returns an empty SpManager.
func NewSpManager(session *mgo.Session) *SpManager {
	spManager := &SpManager{}
	spManager.g_session = session
	spManager.mongoManager = mongo.NewMongoManager()
	spManager.pvManager = pv.NewPvManager(session)
	spManager.opManager = op.NewOpManager(session)
	spManager.pfManager = plateFormat.NewPlateFormatManager(session)
	return spManager
}

func pvFilter(s []pv.Pv, genetrack string, pvtype string, source string, fn func(pv.Pv, string, string, string) bool) []pv.Pv {
	var p []pv.Pv // == nil
	for _, v := range s {
		if fn(v, genetrack, pvtype, source) {
			p = append(p, v)
		}
	}
	return p
}

func matchPv(pvEntry pv.Pv, geneTracking string, pvtype string, source string) bool {
	typematch := false
	if source == "testVariants" {
		if pvEntry.TestType == "colorimetric" || pvEntry.TestType == "hplc" {
			typematch = true
		}
	} else {
		if pvEntry.TestType == source {
			typematch = true
		}
	}
//	return ((pvEntry.Type == pvtype) && (pvEntry.GeneTracking == geneTracking) && (pvEntry.Used == false) && typematch)
	return ((pvEntry.Type == pvtype) && (pvEntry.Used == false) && typematch)

}

func (p *SpManager) generateSp(spc *SpCreator) ([]*Sp, []pv.Pv, error) {
	var firstPlate string
	sppvList := []pv.Pv{}

	if spc.Source == "list" {
		variants := strings.Split(spc.List, "\n")
		if len(variants) == 0 {
			mylog.Warning.Println("variant list is empty")
			return nil, nil, nil
		}
		var oplist []op.Op
		p.opManager.GetOpsByCTNs(variants, &oplist)
		for _,op := range oplist {
			sppvList = append(sppvList, pv.Pv{
				OpTracking: op.Tracking,
				OpWell: op.WellDefinition,
				Colony: op.Colony,
			})
		}
	} else {
		pvList, err := p.pvManager.All(spc.Creator)
		if err != nil {
			mylog.Error.Println("generateSp: can not get pv list")
			return nil, nil, err
		}
		for _, pc := range spc.Pc {
			fList := pvFilter(pvList, pc.Tracking, "sp", spc.Source, matchPv)
			sppvList = append(sppvList, fList...)
		}
	}

	mylog.Debug.Println("sppvList", sppvList)
	lenPvList := len(sppvList)
	if lenPvList == 0 {
		return nil, nil, nil
	}

	// get plate layout format
	var plateLayout plateFormat.Pf
	if err := p.pfManager.GetOnePfById(spc.Layout.Id, &plateLayout); err != nil {
		mylog.Error.Println("generateOp: get plate layout error!")
		return nil, nil, err
	}
	numV := p.opManager.GetVariantsNumber(plateLayout)
	numberPlate := lenPvList / numV
	if lenPvList%numV != 0 {
		numberPlate = numberPlate + 1
	}

	plateTrackingNum, err := tracking.ReserveTracking("sequencePlate", &firstPlate, numberPlate)
	if err != nil {
		mylog.Error.Println("generateSp: ReservedTracking failed!")
		return nil, nil, err
	}

	var spList []*Sp
	exampleTable := plateLayout.Example.([]interface{})
	rows := len(exampleTable)
	row_0 := exampleTable[0]
	columns := len(row_0.([]interface{}))

	wellPerPlate := plateLayout.Nocolumn * plateLayout.Norow
	if plateLayout.Pattern == "byrow" {
		for i := 0; i < numberPlate; i++ {
			for wr := 1; wr < rows; wr++ {
				for wc := 1; wc < columns; wc++ {
					//read sp tracking for each plate
					ind := i*wellPerPlate + (wr-1)*plateLayout.Nocolumn + wc - 1
					if ind < len(sppvList) {
						spTemp := new(Sp)
						pv := sppvList[ind]
						spTemp.Gene = spc.Pc[0]
						if strings.Index(pv.OpTracking, "OP") != -1 {
							origPlate := p.opManager.One(pv.OpTracking)
							spTemp.LibraryName = origPlate.Library.Name
							spTemp.LibraryTracking = origPlate.Library.Tracking
							spTemp.Project = origPlate.Project
						} else {
							spTemp.Project = spTemp.Gene.Project
						}

						spTemp.Tracking = plateTrackingNum[i]
						spTemp.Colony = pv.Colony
						spTemp.OpTracking = pv.OpTracking
						spTemp.OpWell = pv.OpWell
						spTemp.Amount = spc.Amount
						spTemp.Type = "Sequencing"
						spTemp.Format = plateLayout.Name
						spTemp.Creator = spc.Creator
						row_s := exampleTable[wr].([]interface{})
						row_0 := exampleTable[0].([]interface{})
						spTemp.WellDefinition = row_s[0].(string) + row_0[wc].(string)
						spTemp.WellCode = spTemp.Tracking + "-" + spTemp.WellDefinition
						spTemp.Date = time.Now().String()
						spTemp.Notes = spc.Notes
						spTemp.DataType = "variant"
						spList = append(spList, spTemp)
					}
				}
			}
		}
	} else {
		for i := 0; i < numberPlate; i++ {
			for wc := 1; wc < columns; wc++ {
				for wr := 1; wr < rows; wr++ {
					//read sp tracking for each plate
					ind := i*wellPerPlate + (wc-1)*plateLayout.Norow + wr - 1
					if ind < len(sppvList) {
						spTemp := new(Sp)
						pv := sppvList[ind]
						spTemp.Gene = spc.Pc[0]

						if strings.Index(pv.OpTracking, "OP") != -1 {
							origPlate := p.opManager.One(pv.OpTracking)
							spTemp.LibraryName = origPlate.Library.Name
							spTemp.LibraryTracking = origPlate.Library.Tracking
							spTemp.Project = origPlate.Project
						} else {
							spTemp.Project = spTemp.Gene.Project
						}

						spTemp.Tracking = plateTrackingNum[i]
						spTemp.Colony = pv.Colony
						spTemp.OpTracking = pv.OpTracking
						spTemp.OpWell = pv.OpWell
						spTemp.Amount = spc.Amount
						spTemp.Type = "Sequencing"
						spTemp.Format = plateLayout.Name
						spTemp.Creator = spc.Creator
						row_s := exampleTable[wr].([]interface{})
						row_0 := exampleTable[0].([]interface{})
						spTemp.WellDefinition = row_s[0].(string) + row_0[wc].(string)
						spTemp.WellCode = spTemp.Tracking + "-" + spTemp.WellDefinition
						spTemp.Date = time.Now().String()
						spTemp.Notes = spc.Notes
						spTemp.DataType = "variant"
						spList = append(spList, spTemp)
					}
				}
			}
		}
	}

	return spList, sppvList, nil
}

// Save the given Sp in the SpManager.
func (p *SpManager) Save(spc *SpCreator) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", spc)

	if sps, pvList, err := p.generateSp(spc); err == nil {
		for _, spi := range sps {
			//update database
			//log.Println("--each sp", spi)
			err := p.CreateSp(spi)
			if err == nil {
				// add to list in memory
				_ = append(p.sps, spi)
				mylog.Info.Println("Save: createSp succeed")
			} else {
				mylog.Error.Println("Save: createSp failed %v", err)
				return err
			}
		}
		//update pvList.Used = true
		for i, _ := range pvList {
			pvList[i].Used = true
			p.pvManager.UpdatePv(&pvList[i])
		}
	} else {
		mylog.Error.Println("Save: generate sp data failed!")
		return err
	}
	return nil
}

// All returns the list of all the Sps in the SpManager.
func (p *SpManager) All() ([]Sp, error) {
	var sps []Sp
	err := p.GetAllSp(&sps)
	return sps, err
}

// Find returns the Sp with the given id in the SpManager
func (p *SpManager) One(tracking string) Sp {
	if tracking == "" {
		return Sp{}
	}
	for _, t := range p.sps {
		if t.Tracking == tracking {
			return *t
		}
	}

	sp := new(Sp)
	if err := p.GetOneSp(tracking, sp); err == nil {
		p.sps = append(p.sps, sp)
	}
	return *sp
}

func (p *SpManager) CreateSp(sp *Sp) error {
	col := "sp"
	sp.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, sp)
	if err != nil {
		mylog.Error.Println("ERR: Insert sp failed")
		return err
	}
	mylog.Info.Println("CreateSp, successfully insert to mongo")
	// Read sp back
	var readSp Sp
	err1 := p.GetOneSp(sp.Tracking, &readSp)
	if err1 != nil {
		mylog.Error.Println("CreateSp, read back failed")
	} else {
		mylog.Info.Println("CreateSp, successfully read back %s", readSp)
	}

	return nil
}

func (p *SpManager) RemoveSp(sp *Sp) error {
	col := "sp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", sp.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: Insert sp failed")
		return err
	}
	return nil
}

func (p *SpManager) UpdateSp(sp *Sp) error {
	col := "sp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, sp, "tracking", sp.Tracking)
}

func (p *SpManager) GetOneSp(tracking string, sp *Sp) error {
	col := "sp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, sp)
	if err != nil {
		mylog.Error.Println("ERR: query one sp failed")
	}
	return err
}

func (p *SpManager) GetAllSp(sps *[]Sp) error {
	col := "sp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetAllObject(col, "tracking", sps)
	if err != nil {
		mylog.Error.Println("ERR: query sps failed")
	}
	return err
}

func (p *SpManager) GetAllSpByTracking(spTracking string, sps *[]Sp) error {
	col := "sp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", spTracking, sps)
	if err != nil {
		mylog.Error.Println("ERR: query sps failed")
	}
	return err
}
