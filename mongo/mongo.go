package mongo

import (
	"bitbucket.org/bfs/mylog"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"os"
)

type MongoManager struct {
	sess *mgo.Session
	db   string "bfsAdmin"
}

func NewMongoManager() *MongoManager {
	return &MongoManager{}
}

func (m *MongoManager) CreateSession(url string) {
	s, err := mgo.Dial(url)
	if err != nil {
		mylog.Error.Println("Can't connect to mongo, go error %v\n", err)
		os.Exit(1)
	}
	m.sess = s
}

func (m *MongoManager) CopySession(session *mgo.Session) {
	m.sess = session.Copy()
}

func (m *MongoManager) CloseSession() {
	if m.sess != nil {
		m.sess.Close()
	}
}

func (m *MongoManager) BuildIndex(col string, keys []string) error {
	index := mgo.Index{
		Key:        keys,
		Background: true,
		Sparse:     true,
	}
	err := m.sess.DB(m.db).C(col).EnsureIndex(index)
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoManager) CreateObject(col string, object interface{}) error {
	// Write to mongo
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Insert(object)
	if err != nil {
		mylog.Error.Println("ERR: Insert object failed")
		return err
	}
	return nil
}

func (m *MongoManager) RemoveObject(col string, identifier string, value string) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Remove(bson.M{identifier: value})
	if err != nil {
		mylog.Error.Println("ERR: remove object failed")
		return err
	}
	return nil
}

func (m *MongoManager) RemoveAllObject(col string, identifier string, value string) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	_, err := m.sess.DB(m.db).C(col).RemoveAll(bson.M{identifier: value})
	if err != nil {
		mylog.Error.Println("ERR: remove object failed")
		return err
	}
	return nil
}

func (m *MongoManager) RemoveObjectById(col string, value bson.ObjectId) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Remove(bson.M{"_id": value})
	if err != nil {
		mylog.Error.Println("ERR: remove object failed")
		return err
	}
	return nil
}

func (m *MongoManager) UpdateObjectById(col string, object interface{}, value bson.ObjectId) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Update(bson.M{"_id": value}, object)
	if err != nil {
		mylog.Error.Println("ERR: update object failed")
		return err
	}
	return nil
}

func (m *MongoManager) UpdateObject(col string, object interface{}, identifier string, value string) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	changeInfo, err := m.sess.DB(m.db).C(col).UpdateAll(bson.M{identifier: value}, object)
	if err != nil {
		mylog.Error.Println("ERR: update object failed")
		return err
	}
	mylog.Info.Println("ERR: update number of object ", changeInfo.Updated)
	return nil
}

func (m *MongoManager) UpSertObject(col string, object interface{}, identifier string, value string) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	changeInfo, err := m.sess.DB(m.db).C(col).Upsert(bson.M{identifier: value}, object)
	if err != nil {
		mylog.Error.Println("ERR: update object failed")
		return err
	}
	mylog.Info.Println("ERR: update number of object ", changeInfo.Updated)
	return nil
}



func (m *MongoManager) GetSelectedObject(col string, identifier string, value string, result interface{}) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{identifier: value}).All(result)
	if err != nil {
		mylog.Error.Println("ERR: query one object failed, identifier: %s, value: %s", identifier, value)
	}
	return err
}

func (m *MongoManager) GetOneObject(col string, identifier string, value string, result interface{}) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{identifier: value}).One(result)
	if err != nil {
		mylog.Error.Println("ERR: query one object failed, identifier: %s, value: %s", identifier, value)
	}
	return err
}

/*
func (m *MongoManager)GetEmbeddedObjects(col string, idP string, identifier string, value string, result interface{})  error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{idP: {identifier: value}}).One(result)
	if err != nil {
		mylog.Error.Println("ERR: query one object failed")
	}
	return err
}
*/

func (m *MongoManager) GetOneObjectById(col string, value bson.ObjectId, result interface{}) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{"_id": value}).One(result)
	if err != nil {
		mylog.Error.Println("ERR: query one object failed")
	}
	return err
}

func (m *MongoManager) GetAllObject(col string, identifier string, results interface{}) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{}).Sort(identifier).All(results)
	if err != nil {
		mylog.Error.Println("ERR: query object failed")
	}
	return err
}

func (m *MongoManager) GetMulObject(col string, identifier string, values []string, results interface{}) error {
	if m.sess == nil {
		mylog.Error.Println("ERR: mongo session is nil")
		return errors.New("nil mongo session")
	}
	err := m.sess.DB(m.db).C(col).Find(bson.M{identifier: bson.M {"$in":values} }).All(results)
	if err != nil {
		mylog.Error.Println("ERR: query object failed")
	}
	return err
}
