package vp

import (
	"bitbucket.org/bfs/gene"
	"bitbucket.org/bfs/gv"
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/op"
	"bitbucket.org/bfs/plateFormat"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/pv"
	"bitbucket.org/bfs/tracking"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"strings"
	"time"
)

type Vp struct {
	Id              bson.ObjectId   `json:"id" bson:"_id"`
	Tracking        string          `json:"tracking" bson:"tracking"`
	Project         project.Project `json:"project" bson:"project"`
	Gene            gene.Gene       `json:"gene" bson:"gene"`
	LibraryTracking string          `json:"libraryTracking" bson:"libraryTracking"`
	LibraryName     string          `json:"libraryName" bson:"libraryName"`
	Type            string          `json:"type" bson:"type"`
	Format          string          `json:"format" bson:"format"`
	WellDefinition  string          `json:"wellDefinition" bson:"wellDefinition"`
	WellCode        string          `json:"wellCode" bson:"wellCode"`
	Colony          string          `json:"colony" bson:"colony"`
	DataType        string          `json:"dataType" bson:"dataType"`
	Creator         string          `json:"creator" bson:"creator"`
	Date            string          `json:"date" bson:"date"`
	Result          interface{}     `json:"result" bson:"result"`
	Others          interface{}     `json:"others" bson:"others"`
	Notes           string          `json:"notes" bson:"notes"`
	OpTracking      string          `json:"opTracking" bson:"opTracking"`
	OpWell          string          `json:"opWell" bson:"opWell"`
	Amount          int             `json:"amount" bson:"amount"`
	Source          string          `json:"source" bson:"source"`
}

type VpCreator struct {
	NumberReps  string          `json:"numberReps" bson:"numberReps"`
	ControlType string          `json:"controlType" bson:"controlType"`
	Project     project.Project `json:"project" bson:"project"`
	Pc          []gene.Gene     `json:"pc" bson:"pc"`
	Nc          []gene.Gene     `json:"nc" bson:"nc"`
	Amount      int             `json:"amount" bson:"amount"`
	Notes       string          `json:"notes" bson:"notes"`
	Layout      plateFormat.Pf  `json:"layout" bson:"layout"`
	Creator     string          `json:"creator" bson:"creator"`
	Source      string          `json:"source" bson:"source"`
	List        string          `json:"list" bson:"list"`
}

const wellPerPlate = 96
const variableWell = 84
const wellPerRow = 12
const numberOfRow = 8
const controlRow = 'D'

// VpManager manages a list of vps in memory.
type VpManager struct {
	g_session    *mgo.Session
	vps          []*Vp
	pvManager    *pv.PvManager
	gvManager    *gv.GvManager
	opManager    *op.OpManager
	mongoManager *mongo.MongoManager
	pfManager    *plateFormat.PlateFormatManager
	geneManager  *gene.GeneManager
}

// NewVpManager returns an empty VpManager.
func NewVpManager(session *mgo.Session) *VpManager {
	vpManager := &VpManager{}
	vpManager.g_session = session
	vpManager.mongoManager = mongo.NewMongoManager()
	vpManager.pvManager = pv.NewPvManager(session)
	vpManager.gvManager = gv.NewGvManager(session)
	vpManager.opManager = op.NewOpManager(session)
	vpManager.geneManager = gene.NewGeneManager(session)
	vpManager.pfManager = plateFormat.NewPlateFormatManager(session)
	return vpManager
}

func pvFilter(s []pv.Pv, genetrack string, pvtype string, source string, fn func(pv.Pv, string, string, string) bool) []pv.Pv {
	var p []pv.Pv // == nil
	for _, v := range s {
		if fn(v, genetrack, pvtype, source) {
			p = append(p, v)
		}
	}
	return p
}

func matchPv(pvEntry pv.Pv, geneTracking string, pvtype string, source string) bool {
	typematch := false
	if source == "testVariants" {
		if pvEntry.TestType == "colorimetric" || pvEntry.TestType == "hplc" {
			typematch = true
		}
	} else {
		if pvEntry.TestType == source {
			typematch = true
		}
	}
	//return ((pvEntry.Type == pvtype) && (pvEntry.GeneTracking == geneTracking) && (pvEntry.Used == false) && typematch)
	return ((pvEntry.Type == pvtype) && (pvEntry.Used == false) && typematch)
}

func (p *VpManager) generateVp(vpc *VpCreator) ([]Vp, []pv.Pv, []gv.Gv, error) {
	var firstPlate string
	var vListLen int
	var vppvList []pv.Pv
	var gvList []gv.Gv
	var gvErr error

	if vpc.Source == "list" {
		variants := strings.Split(vpc.List, "\n")
		if len(variants) == 0 {
			mylog.Warning.Println("variant list is empty")
			return nil, nil, nil, nil
		}
		var oplist []op.Op
		p.opManager.GetOpsByCTNs(variants, &oplist)
		for _,op := range oplist {
			vppvList = append(vppvList, pv.Pv{
				OpTracking: op.Tracking,
				OpWell: op.WellDefinition,
				Colony: op.Colony,
			})
		}
		vListLen = len(vppvList)
		if vListLen == 0 {
			return nil, nil, nil, nil
		}
	} else if vpc.Source != "geneVariants" {
		pvList, err := p.pvManager.All(vpc.Creator)
		if err != nil {
			mylog.Error.Println("generateVp: can not get pv list")
			return nil, nil, nil, err
		}
		mylog.Debug.Println("pvList", pvList)

		for _, pc := range vpc.Pc {
			fList := pvFilter(pvList, pc.Tracking, "vp", vpc.Source, matchPv)
			vppvList = append(vppvList, fList...)
		}

		vListLen = len(vppvList)
		if vListLen == 0 {
			return nil, nil, nil, nil
		}
	} else {
		gvList, gvErr = p.gvManager.All(vpc.Creator)
		if gvErr != nil {
			mylog.Error.Println("generateVpFromGenes: can not get gv list")
			return nil, nil, nil, gvErr
		}
		vListLen = len(gvList)

	}
	// get plate layout format
	var plateLayout plateFormat.Pf
	if err := p.pfManager.GetOnePfById(vpc.Layout.Id, &plateLayout); err != nil {
		mylog.Error.Println("generateOp: get plate layout error!")
		return nil, nil, nil, err
	}

	numberReps := plateLayout.Replica
	numV := p.opManager.GetVariantsNumber(plateLayout)
	numberPlate := vListLen / numV
	if vListLen%numV != 0 {
		numberPlate = numberPlate + 1
	}

	mylog.Debug.Println("VP-numberPlate:", numberPlate)

	plateTrackingNum, err := tracking.ReserveTracking("validationPlate", &firstPlate, numberPlate)
	if err != nil {
		mylog.Error.Println("generateVp: ReservedTracking failed!")
		return nil, nil, nil, err
	}

	var vpList []Vp
	exampleTable := plateLayout.Example.([]interface{})
	rows := len(exampleTable)
	row_0 := exampleTable[0]
	columns := len(row_0.([]interface{}))
	colTrackInd := 0

	for i := 0; i < numberPlate; i++ {
		for wr := 1; wr < rows; wr++ {
			for wc := 1; wc < columns; wc++ {
				var vpTemp Vp
				vpTemp.Tracking = plateTrackingNum[i]
				vpTemp.Project = vpc.Project
				vpTemp.Type = "Validation"
				vpTemp.Format = plateLayout.Name
				row_s := exampleTable[wr].([]interface{})
				row_0 := exampleTable[0].([]interface{})
				vpTemp.WellDefinition = row_s[0].(string) + row_0[wc].(string)
				vpTemp.WellCode = vpTemp.Tracking + "-" + vpTemp.WellDefinition
				vpTemp.Creator = vpc.Creator
				vpTemp.LibraryName = ""
				vpTemp.LibraryTracking = ""
				vpTemp.Date = time.Now().String()
				vpTemp.Notes = vpc.Notes

				if strings.Contains(row_s[wc].(string), "PosC") {
					cIndex := strings.Trim(row_s[wc].(string), "PosC")
					if index, err := strconv.Atoi(cIndex); err == nil {
						vpTemp.Colony = vpc.Pc[index-1].Colony
						vpTemp.DataType = "Positive Control"
						vpTemp.Gene = vpc.Pc[index-1]
					}
					vpList = append(vpList, vpTemp)
				} else if strings.Contains(row_s[wc].(string), "NegC") {
					cIndex := strings.Trim(row_s[wc].(string), "NegC")
					if index, err := strconv.Atoi(cIndex); err == nil {
						vpTemp.Colony = vpc.Nc[index-1].Colony
						vpTemp.DataType = "Negative Control"
						vpTemp.Gene = vpc.Nc[index-1]
					}
					vpList = append(vpList, vpTemp)
				} else {
					if (colTrackInd / numberReps) < vListLen {
						if vpc.Source == "testVariants" || vpc.Source == "joinedTest" || vpc.Source == "list" {
							pv := vppvList[colTrackInd/numberReps]
							vpTemp.Colony = pv.Colony
							vpTemp.OpTracking = pv.OpTracking
							vpTemp.OpWell = pv.OpWell
							if strings.Index(pv.OpTracking, "OP") != -1 {
								origPlate := p.opManager.One(pv.OpTracking)
								vpTemp.LibraryName = origPlate.Library.Name
								vpTemp.LibraryTracking = origPlate.Library.Tracking
							}
							vpTemp.Gene = vpc.Pc[0]
						} else {
							gv := gvList[colTrackInd/numberReps]
							vpTemp.Colony = gv.Colony
							vpTemp.Gene = p.geneManager.One(gv.GeneTracking)
						}
						vpTemp.Amount = vpc.Amount
						vpTemp.DataType = "variant"

						colTrackInd = colTrackInd + 1
						vpList = append(vpList, vpTemp)
					}
				}
			}
		}
	}
	return vpList, vppvList, gvList, nil
}

// Save the given Vp in the VpManager.
func (p *VpManager) Save(vpc *VpCreator) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", vpc)

	if vps, pvList, gvList, err := p.generateVp(vpc); err == nil {
		for _, vpi := range vps {
			//update database
			//log.Println("--each vp", vpi)
			err := p.CreateVp(&vpi)
			if err == nil {
				// add to list in memory
				//_ = append(p.vps, &vpi)
				mylog.Info.Println("Save: createVp succeed")
			} else {
				mylog.Error.Println("Save: createVp failed %v", err)
				//return err
			}
		}
		//update pvList.Used = true
		for i, _ := range pvList {
			pvList[i].Used = true
			p.pvManager.UpdatePv(&pvList[i])
		}
		for _, gv := range gvList {
			gv.Used = true
			p.gvManager.UpdateGv(&gv)
		}
	} else {
		mylog.Error.Println("Save: generate vp data failed!")
		return err
	}
	return nil
}

// All returns the list of all the Vps in the VpManager.
func (p *VpManager) All() ([]Vp, error) {
	var vps []Vp
	err := p.GetAllVp(&vps)
	return vps, err
}

func (p *VpManager) AllByGeneTracking(tracking string) ([]Vp, error) {
	col := "vp"
	var vps []Vp
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "gene.tracking", tracking, &vps)
	if err != nil {
		mylog.Error.Println("ERR: query colorimetrics failed", tracking)
	}
	return vps, err
}

func (p *VpManager) AllByTracking(tracking string) ([]Vp, error) {
	col := "vp"
	var vps []Vp
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", tracking, &vps)
	if err != nil {
		mylog.Error.Println("ERR: query colorimetrics failed", tracking)
	}
	return vps, err
}

// Find returns the Vp with the given id in the VpManager
func (p *VpManager) One(tracking string) Vp {
	if tracking == "" {
		return Vp{}
	}
	for _, t := range p.vps {
		if t.Tracking == tracking {
			return *t
		}
	}

	vp := new(Vp)
	if err := p.GetOneVp(tracking, vp); err == nil {
		p.vps = append(p.vps, vp)
	}
	return *vp
}

func (p *VpManager) CreateVp(vp *Vp) error {
	col := "vp"
	//vp.Id = bson.NewObjectId()
	// Write to mongo
	vp.Id = bson.NewObjectId()
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, vp)
	if err != nil {
		mylog.Error.Println("ERR: Insert vp failed", vp)
		return err
	}
	mylog.Info.Println("CreateVp, successfully insert to mongo", vp)
	// Read vp back
	var readVp Vp
	err1 := p.GetOneVp(vp.Tracking, &readVp)
	if err1 != nil {
		mylog.Error.Println("CreateVp, read back failed")
	} else {
		mylog.Info.Println("CreateVp, successfully read back %s", readVp)
	}

	return nil
}

func (p *VpManager) RemoveVp(vp *Vp) error {
	col := "vp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", vp.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: Insert vp failed")
		return err
	}
	return nil
}

func (p *VpManager) UpdateVp(vp *Vp) error {
	col := "vp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, vp, "tracking", vp.Tracking)
}

func (p *VpManager) GetOneVp(tracking string, vp *Vp) error {
	col := "vp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, vp)
	if err != nil {
		mylog.Error.Println("ERR: query one vp failed")
	}
	return err
}

func (p *VpManager) GetAllVp(vps *[]Vp) error {
	col := "vp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetAllObject(col, "tracking", vps)
	if err != nil {
		mylog.Error.Println("ERR: query vps failed")
	}
	return err
}

func (p *VpManager) GetAllVpByTracking(vpTracking string, vps *[]Vp) error {
	col := "vp"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", vpTracking, vps)
	if err != nil {
		mylog.Error.Println("ERR: query vps failed")
	}
	return err
}
