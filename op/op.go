package op

import (
	"bitbucket.org/bfs/gene"
	"bitbucket.org/bfs/library"
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/plateFormat"
	"bitbucket.org/bfs/project"
	"bitbucket.org/bfs/tracking"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strconv"
	"strings"
	"time"
)

type Op struct {
	Id             bson.ObjectId   `json:"id" bson:"_id"`
	Tracking       string          `json:"tracking" bson:"tracking"`
	Project        project.Project `json:"project" bson:"project"`
	Library        library.Library `json:"library" bson:"library"`
	Type           string          `json:"type" bson:"type"`
	Format         string          `json:"format" bson:"format"`
	WellDefinition string          `json:"wellDefinition" bson:"wellDefinition"`
	WellCode       string          `json:"wellCode" bson:"wellCode"`
	Colony         string          `json:"colony" bson:"colony"`
	DataType       string          `json:"dataType" bson:"dataType"`
	Creator        string          `json:"creator" bson:"creator"`
	Date           string          `json:"date" bson:"date"`
	Result         interface{}     `json:"result" bson:"result"`
	Others         interface{}     `json:"others" bson:"others"`
	Notes          string          `json:"notes" bson:"notes"`
	Selected       bool            `json:"selected" bson:"selected"`
}

type OpCreator struct {
	Project project.Project `json:"project" bson:"project"`
	Library library.Library `json:"library" bson:"library"`
	Number  int             `json:"number" bson:"number"`
	Pc      []gene.Gene     `json:"pc" bson:"pc"`
	Nc      []gene.Gene     `json:"nc" bson:"nc"`
	Notes   string          `json:"notes" bson:"notes"`
	Layout  plateFormat.Pf  `json:"layout" bson:"layout"`
	Creator string
}

// OpManager manages a list of ops in memory.
type OpManager struct {
	g_session    *mgo.Session
	ops          []*Op
	mongoManager *mongo.MongoManager
	pfManager    *plateFormat.PlateFormatManager
}

// NewOpManager returns an empty OpManager.
func NewOpManager(session *mgo.Session) *OpManager {
	opManager := &OpManager{}
	opManager.g_session = session
	opManager.mongoManager = mongo.NewMongoManager()
	opManager.pfManager = plateFormat.NewPlateFormatManager(session)
	//opManager.mongoManager.BuildIndex("op", []string{"tracking", "library.tracking"})
	return opManager
}

func (p *OpManager) GetVariantsNumber(plateLayout plateFormat.Pf) int {
	exampleTable := plateLayout.Example.([]interface{})
	rows := len(exampleTable)
	row_0 := exampleTable[0]
	columns := len(row_0.([]interface{}))

	//calculate number of colony to reserve
	vColonyNumPerPlate := 0
	for ri := 1; ri < rows; ri++ {
		for ci := 1; ci < columns; ci++ {
			row_item := exampleTable[ri].([]interface{})
			item := row_item[ci].(string)
			if strings.Contains(item, "PosC") {
				fmt.Println(row_item[ci].(string))
			} else if strings.Contains(item, "NegC") {
				fmt.Println(row_item[ci].(string))
			} else {
				vColonyNumPerPlate++
			}
		}
	}
	vColonyNumPerPlate = vColonyNumPerPlate / plateLayout.Replica

	return vColonyNumPerPlate
}

func (p *OpManager) generateOp(opc *OpCreator) ([]*Op, error) {
	var firstColony string
	var firstPlate string

	plateTrackingNum, err := tracking.ReserveTracking("originalPlate", &firstPlate, opc.Number)
	if err != nil {
		mylog.Error.Println("generateOp: ReservedTracking failed!")
		return nil, err
	}
	// get plate layout format
	var plateLayout plateFormat.Pf
	if err := p.pfManager.GetOnePfById(opc.Layout.Id, &plateLayout); err != nil {
		mylog.Error.Println("generateOp: get plate layout error!")
		return nil, err
	}

	vColonyNumPerPlate := p.GetVariantsNumber(plateLayout)

	colonyTrackingNum, err := tracking.ReserveTracking("colony", &firstColony, vColonyNumPerPlate*opc.Number)
	if err != nil {
		mylog.Error.Println("generateOp: ReserveTracking for colony failed!")
		return nil, err
	}

	log.Println(colonyTrackingNum)

	wellPerPlate := plateLayout.Norow * plateLayout.Nocolumn
	opList := make([]*Op, wellPerPlate*opc.Number)
	exampleTable := plateLayout.Example.([]interface{})
	rows := len(exampleTable)
	row_0 := exampleTable[0]
	columns := len(row_0.([]interface{}))

	for i := 0; i < opc.Number; i++ {
		for wr := 1; wr < rows; wr++ {
			for wc := 1; wc < columns; wc++ {
				ind := i*wellPerPlate + (wr-1)*plateLayout.Nocolumn + wc - 1
				opTemp := new(Op)

				opTemp.Tracking = plateTrackingNum[i]
				opTemp.Project = opc.Project
				opTemp.Library = opc.Library
				opTemp.Type = "original"
				opTemp.Format = plateLayout.Name
				opTemp.Creator = opc.Creator
				row_s := exampleTable[wr].([]interface{})
				row_0 := exampleTable[0].([]interface{})

				opTemp.WellDefinition = row_s[0].(string) + row_0[wc].(string)
				opTemp.WellCode = opTemp.Tracking + "-" + opTemp.WellDefinition

				if strings.Contains(row_s[wc].(string), "PosC") {
					cIndex := strings.Trim(row_s[wc].(string), "PosC")
					if index, err := strconv.Atoi(cIndex); err == nil {
						opTemp.Colony = opc.Pc[index-1].Colony
						opTemp.DataType = "Positive Control"
					}
				} else if strings.Contains(row_s[wc].(string), "NegC") {
					cIndex := strings.Trim(row_s[wc].(string), "NegC")
					if index, err := strconv.Atoi(cIndex); err == nil {
						opTemp.Colony = opc.Nc[index-1].Colony
						opTemp.DataType = "Negative Control"
					}
				} else {
					opTemp.DataType = "variant"
					ci, _ := strconv.Atoi(row_s[wc].(string))
					row_1 := exampleTable[1].([]interface{})
					fi, _ := strconv.Atoi(row_1[1].(string))
					//calculate the current position offside to the first well plus plate number
					offside := ci - fi + i*vColonyNumPerPlate
					opTemp.Colony = colonyTrackingNum[offside]
				}
				opTemp.Date = time.Now().String()
				opTemp.Notes = opc.Notes
				opTemp.Selected = false

				opList[ind] = opTemp
			}
		}
	}
	return opList, nil
}

// Save the given Op in the OpManager.
func (p *OpManager) Save(opc *OpCreator) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", opc)

	if ops, err := p.generateOp(opc); err == nil {
		for _, opi := range ops {
			//update database
			//log.Println("--each op", opi)
			err := p.CreateOp(opi)
			if err == nil {
				// add to list in memory
				_ = append(p.ops, opi)
				mylog.Info.Println("Save: createOp succeed")
			} else {
				mylog.Error.Println("Save: createOp failed %v", err)
				return err
			}
		}
	} else {
		mylog.Error.Println("Save: generate op data failed!")
		return err
	}
	return nil
}

// All returns the list of all the Ops in the OpManager.
func (p *OpManager) All() ([]Op, error) {
	var ops []Op
	err := p.GetAllOp(&ops)
	return ops, err
}

// All returns the list of all the Ops in the OpManager.
func (p *OpManager) AllByLib(libTracking string) ([]Op, error) {
	var ops []Op
	err := p.GetAllOpByLib(libTracking, &ops)
	return ops, err
}

// Find returns the Op with the given id in the OpManager
func (p *OpManager) One(tracking string) Op {
	if tracking == "" {
		return Op{}
	}
	for _, t := range p.ops {
		if t.Tracking == tracking {
			return *t
		}
	}

	op := new(Op)
	if err := p.GetOneOp(tracking, op); err == nil {
		p.ops = append(p.ops, op)
	}
	return *op
}

func (p *OpManager) CreateOp(op *Op) error {
	col := "op"
	op.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, op)
	if err != nil {
		mylog.Error.Println("ERR: Insert op failed")
		return err
	}
	mylog.Info.Println("CreateOp, successfully insert to mongo")
	// Read op back
	var readOp Op
	err1 := p.GetOneOp(op.Tracking, &readOp)
	if err1 != nil {
		mylog.Error.Println("CreateOp, read back failed")
	} else {
		mylog.Info.Println("CreateOp, successfully read back %s", readOp)
	}

	return nil
}

func (p *OpManager) RemoveOp(op *Op) error {
	col := "op"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "tracking", op.Tracking)
	if err != nil {
		mylog.Error.Println("ERR: Insert op failed")
		return err
	}
	return nil
}

func (p *OpManager) UpdateOp(op *Op) error {
	col := "op"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObject(col, op, "tracking", op.Tracking)
}

func (p *OpManager) GetOneOp(tracking string, op *Op) error {
	col := "op"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, op)
	if err != nil {
		mylog.Error.Println("ERR: query one op failed")
	}
	return err
}

func  (p *OpManager) GetOpsByCTNs(colonies []string, ops *[]Op) error {
	col := "op"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetMulObject(col, "colony", colonies, ops)
	if err != nil {
		mylog.Error.Println("ERR: query ops failed")
	}
	return err
}


func (p *OpManager) GetAllOp(ops *[]Op) error {
	col := "op"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetAllObject(col, "_id", ops)
	if err != nil {
		mylog.Error.Println("ERR: query ops failed")
	}
	return err
}

func (p *OpManager) GetAllOpByTracking(opTracking string, ops *[]Op) error {
	col := "op"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", opTracking, ops)
	if err != nil {
		mylog.Error.Println("ERR: query ops failed", opTracking)
	}
	return err
}

func (p *OpManager) GetAllOpByLib(libTracking string, ops *[]Op) error {
	col := "op"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "library.tracking", libTracking, ops)
	if err != nil {
		mylog.Error.Println("ERR: query ops failed", libTracking)
	}
	return err
}
