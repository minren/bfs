package mylog

import (
	"fmt"
	"github.com/natefinch/lumberjack"
	"io"
	"log"
	"os"
	"time"
)

const errLogFile = "./bfs_err.log"
const warnLogFile = "./bfs_warn.log"
const debugLogFile = "./bfs_debug.log"
const infoLogFile = "./bfs_info.log"

var (
	Debug   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
)

func logInit(
	debugHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	Debug = log.New(&lumberjack.Logger{
		Filename:   debugLogFile,
		MaxSize:    400,
		MaxBackups: 10,
		MaxAge:     1,
	}, "", 0)

	Info = log.New(&lumberjack.Logger{
		Filename:   infoLogFile,
		MaxSize:    100,
		MaxBackups: 10,
		MaxAge:     1,
	}, "", 0)

	Warning = log.New(&lumberjack.Logger{
		Filename:   warnLogFile,
		MaxSize:    100,
		MaxBackups: 10,
		MaxAge:     1,
	}, "", 0)

	Error = log.New(&lumberjack.Logger{
		Filename:   errLogFile,
		MaxSize:    100,
		MaxBackups: 10,
		MaxAge:     1,
	}, "", 0)
	Debug.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " DEBUG ")
	Info.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " INFO ")
	Warning.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " WARN ")
	Error.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " ERROR ")
}

func LogSetup() {
	// set log
	infoFile, err := os.OpenFile(infoLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Open infoLog file failed", err)
		os.Exit(1)
	}
	//defer infoFile.Close()

	debugFile, err := os.OpenFile(debugLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Open debugLog file failed", err)
		os.Exit(1)
	}
	//defer debugFile.Close()
	warnFile, err := os.OpenFile(warnLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Open warnLog file failed", err)
		os.Exit(1)
	}
	//defer warnFile.Close()
	errFile, err := os.OpenFile(errLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Open errLog file failed", err)
		os.Exit(1)
	}
	//defer errFile.Close()

	logInit(debugFile, infoFile, warnFile, errFile)
}
