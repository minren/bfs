package main

import (
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/server"
	"flag"
	"gopkg.in/mgo.v2"
	"os"
	"bitbucket.org/bfs/tracking"
)

const mongoDB = "bfsAdmin"

var Trackingfile string

func main() {
	// set log
	mylog.LogSetup()
	mongoUrl := flag.String("dbserver", "127.0.0.1:27017", "mongo db server")
	trackingfile := flag.String("tracking", "tracking.json", "tracking track json file")
	port := flag.String("port", "8081", "bfs listen port")
	rootDir := flag.String("root", "static", "resource root dir")
	authFlag := flag.Bool("auth", false, "enable authentication")
	user := flag.String("user", "", "username if enable authentication")
	passwd := flag.String("pwd", "", "password if enable authentication")

	flag.Parse()
	// create mongo global session
	tracking.TrackingFile = *trackingfile
	murl := *mongoUrl + "/test"
	mongoSession, err := mgo.Dial(murl)
	if err != nil {
		mylog.Error.Printf("Can't connect to mongo, go error %v\n", err)
		os.Exit(1)
	}
	defer mongoSession.Close()

	if *authFlag == true {
		cred := mgo.Credential{
			Username: *user,//"myTester"
			Password: *passwd, //"xyz123",

		}
		if err := mongoSession.Login(&cred); err != nil {
			mylog.Error.Printf("Can't login to mongo, go error %v\n", err)
			return
		}
		defer mongoSession.LogoutAll()
	}
	mongoSession.SetMode(mgo.Monotonic, true)

	// instance a new server
	server := server.NewServer(mongoSession)
	go server.DbDataCleanup()
	server.RegisterHandlers(*port, *rootDir)
}
