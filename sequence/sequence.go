package sequence

import (
	"errors"
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Sequence struct {
	Id           bson.ObjectId `json:"id" bson:"_id"`
	Colony       string        `json:"colony" bson:"colony"`
	DnaSeq       string        `json:"dnaSeq" bson:"dnaSeq"`
	ProteinSeq   string        `json:"proteinSeq" bson:"proteinSeq"`
}

type SequenceManager struct {
	g_session    *mgo.Session
	sequences    []*Sequence
	mongoManager *mongo.MongoManager
}

// NewSequenceManager returns an empty SequenceManager.
func NewSequenceManager(session *mgo.Session) *SequenceManager {
	sequenceManager := &SequenceManager{}
	sequenceManager.g_session = session
	sequenceManager.mongoManager = mongo.NewMongoManager()
	return sequenceManager
}

// Save the given Sequence in the SequenceManager.
func (p *SequenceManager) Save(sequences []Sequence, overwrite bool) error {
	mylog.Info.Println("Save: %s", sequences)
	// check if sequence already exist

	var sequenceList []Sequence
	if err := p.GetAllSequence(&sequenceList); err != nil {
		mylog.Error.Println("Save: GetAllSequence failed %v", err)
		return errors.New("GetAllSequence failed")
	}

	curSeqMap := make(map[string]Sequence)
	for _, cseq := range sequenceList {
		curSeqMap[cseq.Colony] = cseq
	}

	for _, sequence := range sequences {
		if es,ok := curSeqMap[sequence.Colony]; ok {
			if !overwrite {
				mylog.Error.Println("Save: the colony's sequence already exist", sequence.Colony)
				return errors.New("the colony's sequence already exist")
			} else {
				//update database
				sequence.Id = es.Id
				if err := p.UpSertSequence(&sequence); err != nil {
					mylog.Error.Println("Save: createSequence failed %v", err)
					return err
				}
			}
		} else {
			//update database
			sequence.Id = bson.NewObjectId()
			if err := p.UpSertSequence(&sequence); err != nil {
				mylog.Error.Println("Save: createSequence failed %v", err)
				return err
			}
		}
	}
	return nil
}


func (p *SequenceManager) CreateSequence(sequence *Sequence) error {
	col := "sequence"
	sequence.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, sequence)
	if err != nil {
		mylog.Error.Println("ERR: Insert sequence failed")
		return err
	}
	mylog.Info.Println("CreateSequence, successfully insert to mongo")

	return nil
}

func (p *SequenceManager) RemoveSequence(sequence *Sequence) error {
	col := "sequence"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObjectById(col, sequence.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove sequence failed")
		return err
	}
	return nil
}

func (p *SequenceManager) UpdateSequence(sequence *Sequence) error {
	col := "sequence"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, sequence, sequence.Id)
}


func (p *SequenceManager) UpSertSequence(sequence *Sequence) error {
	col := "sequence"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpSertObject(col, sequence, "colony", sequence.Colony)
}


func (p *SequenceManager) GetAllSequence(sequences *[]Sequence) error {
	col := "sequence"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "_id", sequences)
	if err != nil {
		mylog.Error.Println("ERR: query sequences failed")
	}
	return err
}

func (p *SequenceManager) All() ([]Sequence, error) {
	var sqs []Sequence
	err := p.GetAllSequence(&sqs)
	return sqs, err
}

func (p *SequenceManager) GetOneSequence(colony string) (Sequence, error) {
	col := "sequence"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	var seq Sequence
	err := p.mongoManager.GetOneObject(col, "colony", colony, &seq)
	if err != nil {
		mylog.Error.Println("ERR: query sequences failed")
		return seq, err
	}
	return seq, nil
}