package gv

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Gv struct {
	Id              bson.ObjectId `json:"id" bson:"_id"`
	GeneTracking    string        `json:"geneTracking" bson:"geneTracking"`
	ProjectTracking string        `json:"projectTracking" bson:"projectTracking"`
	Colony          string        `json:"colony" bson:"colony"`
	Username        string        `json:"username" bson:"username"`
	Used            bool          `json:"used" bson:"used"`
}

type GvManager struct {
	g_session    *mgo.Session
	gvs          []*Gv
	mongoManager *mongo.MongoManager
}

// NewPvManager returns an empty PvManager.
func NewGvManager(session *mgo.Session) *GvManager {
	gvManager := &GvManager{}
	gvManager.g_session = session
	gvManager.mongoManager = mongo.NewMongoManager()
	return gvManager
}

// Save the given Pv in the PvManager.
func (p *GvManager) Save(gvs []Gv) error {
	mylog.Info.Println("Save: %s", gvs)
	for _, gv := range gvs {
		//update database
		if err := p.CreateGv(&gv); err == nil {
			// add to list in memory
			_ = append(p.gvs, &gv)
			mylog.Info.Println("Save: createPv succeed")
		} else {
			mylog.Error.Println("Save: createPv failed %v", err)
			return err
		}
	}
	return nil
}

// All returns the list of all the Pvs in the PvManager.
func (p *GvManager) All(username string) ([]Gv, error) {
	var gvs []Gv
	var validGvs []Gv
	err := p.GetAllGv(&gvs)
	for _, gv := range gvs {
		if gv.Used == false && gv.Username == username {
			validGvs = append(validGvs, gv)
		}
	}
	return validGvs, err
}

func (p *GvManager) CreateGv(gv *Gv) error {
	col := "gv"
	gv.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, gv)
	if err != nil {
		mylog.Error.Println("ERR: Insert gv failed")
		return err
	}
	mylog.Info.Println("CreatePv, successfully insert to mongo")

	return nil
}

func (p *GvManager) RemoveGv(gv *Gv) error {
	col := "gv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObjectById(col, gv.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove gv failed")
		return err
	}
	return nil
}

func (p *GvManager) UpdateGv(gv *Gv) error {
	col := "gv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, gv, gv.Id)
}

func (p *GvManager) GetAllGv(gvs *[]Gv) error {
	col := "gv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "_id", gvs)
	if err != nil {
		mylog.Error.Println("ERR: query gvs failed")
	}
	return err
}
