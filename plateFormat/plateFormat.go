package plateFormat

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

type Pf struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Norow       int           `json:"norow" bson:"norow"`
	Nocolumn    int           `json:"nocolumn" bson:"nocolumn"`
	Replica     int           `json:"replica" bson:"replica"`
	Pattern     string        `json:"pattern" bson:"pattern"`
	Pcs         [][]string    `json:"pcs" bson:"pcs"`
	Ncs         [][]string    `json:"ncs" bson:"ncs"`
	Description string        `json:"description" bson:"description"`
	Date        string        `json:"date" bson:"date"`
	Example     interface{}   `json:"example" bson:"example"`
	Invalid     bool          `json:"invalid"`
}

// ProjectManager manages a list of projects in memory.
type PlateFormatManager struct {
	g_session    *mgo.Session
	pf           []*Pf
	mongoManager *mongo.MongoManager
}

// NewPlateFormatManager returns an empty PlateFormatManager.
func NewPlateFormatManager(session *mgo.Session) *PlateFormatManager {
	projectManager := &PlateFormatManager{}
	projectManager.g_session = session
	projectManager.mongoManager = mongo.NewMongoManager()
	return projectManager
}

// Save the given Project in the ProjectManager.
func (p *PlateFormatManager) Save(pf *Pf) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", pf)
	for _, t := range p.pf {
		if t.Name == pf.Name {
			err := errors.New("Pf name already exist")
			return err
		}
	}
	//update database
	err := p.CreatePf(pf)
	if err == nil {
		// add to list in memory
		_ = append(p.pf, pf)
		mylog.Info.Println("Save: createPf succeed")
	} else {
		mylog.Error.Println("Save: createPf failed %v", err)
	}

	return err
}

// All returns the list of all the Projects in the ProjectManager.
func (p *PlateFormatManager) All() ([]Pf, error) {
	var pfs []Pf
	err := p.GetAllPf(&pfs)
	var validPfs []Pf
	for _, pf := range pfs {
		if pf.Invalid == false {
			validPfs = append(validPfs, pf)
		}
	}
	return validPfs, err
}

// Find returns the Pf with the given name
func (p *PlateFormatManager) One(name string) Pf {
	if name == "" {
		return Pf{}
	}
	for _, t := range p.pf {
		if t.Name == name {
			return *t
		}
	}

	pf := new(Pf)
	if err := p.GetOnePf(name, pf); err == nil {
		p.pf = append(p.pf, pf)
	}
	return *pf
}

func (p *PlateFormatManager) CreatePf(pf *Pf) error {
	col := "pf"
	pf.Id = bson.NewObjectId()
	pf.Date = time.Now().String()
	pf.Invalid = false
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, pf)
	if err != nil {
		mylog.Error.Println("ERR: Insert pf failed")
		return err
	}
	log.Println("CreatePf, successfully insert to mongo")
	// Read pf back
	var readPf Pf
	err1 := p.GetOnePf(pf.Name, &readPf)
	if err1 != nil {
		mylog.Error.Println("CreatePf, read back failed")
	} else {
		mylog.Info.Println("CreatePf, successfully read back %s", readPf)
	}

	return nil
}

func (p *PlateFormatManager) RemovePf(pf *Pf) error {
	col := "pf"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObject(col, "name", pf.Name)
	if err != nil {
		mylog.Error.Println("ERR: remove pf failed")
		return err
	}
	return nil
}

func (p *PlateFormatManager) UpdatePf(pf *Pf) error {
	col := "pf"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, pf, pf.Id)
}

func (p *PlateFormatManager) GetOnePf(name string, pf *Pf) error {
	col := "pf"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "name", name, pf)
	if err != nil {
		mylog.Error.Println("ERR: query one pf failed")
	}
	return err
}

func (p *PlateFormatManager) GetOnePfById(id bson.ObjectId, pf *Pf) error {
	col := "pf"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObjectById(col, id, pf)
	if err != nil {
		mylog.Error.Println("ERR: query one pf failed")
	}
	return err
}

func (p *PlateFormatManager) GetAllPf(pfs *[]Pf) error {
	col := "pf"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "name", pfs)
	if err != nil {
		mylog.Error.Println("ERR: query pf failed")
	}
	return err
}
