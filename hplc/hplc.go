package hplc

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"bitbucket.org/bfs/op"
	"bitbucket.org/bfs/sp"
	"bitbucket.org/bfs/tracking"
	"bitbucket.org/bfs/vp"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"math"
	"strconv"
	"time"
)

type PcResult struct {
	pcAvg float64
	pcStd float64
	pcCv  float64
	pf    float64
}

type HplcRequest struct {
	Name           string              `json:"name"`
	Reactionph     string              `json:"reactionph"`
	Temperature    string              `json:"temperature"`
	Time           string              `json:"time"`
	Dilution       string              `json:"dilution"`
	Notes          string              `json:"notes"`
	Type           string              `json:"type"`
	PlateType      string              `json:"plateType"`
	SelectedColumn string              `json:"selectedColumn"`
	Data           []map[string]string `json:"data"`
	Creator        string
}

type Hplc struct {
	Id              bson.ObjectId     `json:"id" bson:"_id"`
	Tracking        string            `json:"tracking" bson:"tracking"`
	PlateTracking   string            `json:"plateTracking" bson:"plateTracking"`
	ProjectTracking string            `json:"projectTracking" bson:"projectTracking"`
	LibraryName     string            `json:"libraryName" bson:"libraryName"`
	LibraryTracking string            `json:"libraryTracking" bson:"libraryTracking"`
	GeneTracking    string            `json:"geneTracking" bson:"geneTracking"`
	Name            string            `json:"name"`
	Reactionph      string            `json:"reactionph"`
	Temperature     string            `json:"temperature"`
	Time            string            `json:"time"`
	Dilution        string            `json:"dilution"`
	Notes           string            `json:"notes"`
	Type            string            `json:"type"`
	PlateType       string            `json:"plateType"`
	PlateFormat     string            `json:"plateFormat" bson:"plateFormat"`
	WellDefinition  string            `json:"wellDefinition" bson:"wellDefinition"`
	WellCode        string            `json:"wellCode" bson:"wellCode"`
	Colony          string            `json:"colony" bson:"colony"`
	DataType        string            `json:"dataType" bson:"dataType"`
	Creator         string            `json:"creator" bson:"creator"`
	Date            string            `json:"date" bson:"date"`
	Od              map[string]string `json:"od" bson:"od"`
	Pcavg           string            `json:"pcavg" bson:"pcavg"`
	Pcstd           string            `json:"pcstd" bson:"pcstd"`
	Pccv            string            `json:"pccv" bson:"pccv"`
	Pf              string            `json:"pf" bson:"pf"`
	Pcavgn          string            `json:"pcavgn" bson:"pcavgn"`
	Pcstdn          string            `json:"pcstdn" bson:"pcstdn"`
	Pccvn           string            `json:"pccvn" bson:"pccvn"`
	Pfn             string            `json:"pfn" bson:"pfn"`
	PcInvalid       bool              `json:"pcInvalid" bson:"pcInvalid"`
	Selected        bool              `json:"selected"`
	SelectedData    string            `json:"selectedData" bson:"selectedData"`
	SelectedDatan   string            `json:"selectedDatan" bson:"selectedDatan"`
}

// cmManager manages a list of hplcs in memory.
type HplcManager struct {
	g_session    *mgo.Session
	hplcs        []*Hplc
	mongoManager *mongo.MongoManager
	opManager    *op.OpManager
	vpManager    *vp.VpManager
	spManager    *sp.SpManager
}

// NewGeneManager returns an empty cmManager.
func NewHplcManager(session *mgo.Session) *HplcManager {
	hplcManager := &HplcManager{}
	hplcManager.g_session = session
	hplcManager.mongoManager = mongo.NewMongoManager()
	hplcManager.opManager = op.NewOpManager(session)
	hplcManager.vpManager = vp.NewVpManager(session)
	hplcManager.spManager = sp.NewSpManager(session)

	hplcManager.mongoManager.CopySession(session)
	defer hplcManager.mongoManager.CloseSession()

	hplcManager.mongoManager.BuildIndex("hplc", []string{"plateTracking", "tracking", "geneTracking"})
	return hplcManager
}

// All returns the list of all the Ops in the OpManager.
func (p *HplcManager) All() ([]Hplc, error) {
	var hplcs []Hplc
	err := p.GetAllHplcs(&hplcs)
	return hplcs, err
}

func (p *HplcManager) AllByTracking(tracking string) ([]Hplc, error) {
	var hplcs []Hplc
	err := p.GetAllHplcsByTracking(tracking, &hplcs)
	return hplcs, err
}

func (p *HplcManager) AllByGeneTracking(tracking string) ([]Hplc, error) {
	var hplcs []Hplc
	err := p.GetAllHplcsByGeneTracking(tracking, &hplcs)
	return hplcs, err
}

/*
func (p *HplcManager) InvPcUpdateCMs(cms []Hplc) error {
	//convert to map
	invPcMap := make(map[string][]Hplc)
	for _, pc := range cms {
		if pcs, ok := invPcMap[pc.PlateTracking]; ok {
			invPcMap[pc.PlateTracking] = append(pcs, pc)
		} else {
			invPcMap[pc.PlateTracking] = append([]Hplc{}, pc)
		}
	}
	//update db plate by plate

	for plateTracking, invPCs := range invPcMap {
		var cms []Hplc
		// read cms from db
		if err := p.GetAllCMsByPlate(plateTracking, &cms); err != nil {
			return err
		}
		//update PcInvalid field
		for _, invPc := range invPCs {
			for ind, cm := range cms {
				if cm.DataType == "Positive Control" && invPc.WellDefinition == cm.WellDefinition {
					cms[ind].PcInvalid = true
				}
			}
		}

		validPcVal := []float64{}
		rmbPcVal := []float64{}
		ncVal := []float64{}

		for _, validCm := range cms {
			vod, _ := strconv.ParseFloat(validCm.Od, 64)
			if validCm.DataType == "Positive Control" && validCm.PcInvalid == false {
				validPcVal = append(validPcVal, vod)
			} else if validCm.DataType == "Negative Control" {
				ncVal = append(ncVal, vod)
			}
		}

		avgPc := 0.0
		avgNc := 0.0
		pcStd := 0.0
		pcCv := 0.0
		if len(validPcVal) != 0 {
			avgPc = calAvg(validPcVal)
			pcStd = calStd(validPcVal)
			pcCv = calCv(avgPc, pcStd)
		}
		if len(ncVal) != 0 {
			avgNc = calAvg(ncVal)
		}

		for _, pcv := range validPcVal {
			rmbPcVal = append(rmbPcVal, pcv-avgNc)
		}

		for index, cm := range cms {
			cms[index].Pcavg = strconv.FormatFloat(avgPc, 'f', 6, 64)
			cms[index].Pcstd = strconv.FormatFloat(pcStd, 'f', 6, 64)
			cms[index].Pccv = strconv.FormatFloat(pcCv, 'f', 6, 64)
			od, _ := strconv.ParseFloat(cm.Od, 64)
			if len(validPcVal) == 0 && avgNc != 0.0 {
				pf := od / avgNc
				cms[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
				cms[index].Pfn = cms[index].Pf
			} else {
				pf := od / avgPc
				cms[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
				pfn := od / calAvg(rmbPcVal)
				cms[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
			}
		}
		//update cms to db
		if err := p.UpdateAllCMsByPlate(plateTracking, cms); err != nil {
			return err
		}
	}
	return nil
}
*/

func (p *HplcManager) UpdateAllHplcsByPlate(plateTracking string, hplcs []Hplc) error {
	col := "hplc"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	for _, hplc := range hplcs {
		err := p.mongoManager.UpdateObjectById(col, hplc, hplc.Id)
		if err != nil {
			mylog.Error.Println("ERR: update hplcs failed", hplc.Id, hplc.PlateTracking, hplc.WellDefinition, err)
			return err
		}
	}
	return nil
}

func (p *HplcManager) GetAllHplcsByPlate(plateTracking string, cms *[]Hplc) error {
	col := "hplc"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "plateTracking", plateTracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query hplcs failed", plateTracking)
	}
	return err
}

func (p *HplcManager) GetAllHplcsByTracking(tracking string, cms *[]Hplc) error {
	col := "hplc"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "tracking", tracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query hplcs failed", tracking)
	}
	return err
}

func (p *HplcManager) GetAllHplcsByGeneTracking(tracking string, cms *[]Hplc) error {
	col := "hplc"
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetSelectedObject(col, "geneTracking", tracking, cms)
	if err != nil {
		mylog.Error.Println("ERR: query hplcs failed", tracking)
	}
	return err
}

func (p *HplcManager) GetAllHplcs(hplcs *[]Hplc) error {
	col := "hplc"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()
	err := p.mongoManager.GetAllObject(col, "_id", hplcs)
	if err != nil {
		mylog.Error.Println("ERR: query all hplc failed")
	}
	return err
}

func (p *HplcManager) GetOneHPLC(tracking string, hplc *Hplc) error {
	col := "hplc"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetOneObject(col, "tracking", tracking, hplc)
	if err != nil {
		mylog.Error.Println("ERR: query one hplc failed")
	}
	return err
}

// Save the given Hplc in the cmManager.
func (p *HplcManager) Save(request *HplcRequest) error {
	// check if tracking already exist
	mylog.Info.Println("Save: %s", request)
	//update database
	if hplcs, err := p.GenerateHPLC(request); err == nil {
		for _, hplc := range hplcs {
			//update database
			//log.Println("--each op", opi)
			err := p.CreateHPLC(hplc)
			if err == nil {
				// add to list in memory
				_ = append(p.hplcs, hplc)
				mylog.Info.Println("Save: createOp succeed")
			} else {
				mylog.Error.Println("Save: createOp failed %v", err)
				return err
			}
		}
		return nil
	} else {
		mylog.Error.Println("Save: generate op data failed!")
		return err
	}
}

func (p *HplcManager) CreateHPLC(hplc *Hplc) error {
	col := "hplc"
	hplc.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, hplc)
	if err != nil {
		mylog.Error.Println("ERR: Insert hplc failed")
		return err
	}
	mylog.Info.Println("CreateCM, successfully insert to mongo")
	// Read op back
	var readCM Hplc
	err1 := p.GetOneHPLC(hplc.Tracking, &readCM)
	if err1 != nil {
		mylog.Error.Println("CreateCM, read back failed")
	} else {
		mylog.Info.Println("CreateCM, successfully read back %s", readCM)
	}

	return nil
}

func (p *HplcManager) GenerateHPLC(request *HplcRequest) ([]*Hplc, error) {

	var trackNum string

	if _, err := tracking.ReserveTracking("test", &trackNum, 1); err != nil {
		mylog.Error.Println("Create Hplc: ReserveTracking failed!")
		return nil, err
	}
	results := request.Data

	plateMap := make(map[string]interface{})
	pcMap := make(map[string][]float64)
	ncMap := make(map[string][]float64)

	var hplcs []*Hplc

	preTracking := ""
	for _, result := range results {
		for key, value := range result {
			if key == "plateTracking" {
				plateTracking := value
				if plateTracking != preTracking {
					if request.PlateType == "op" {
						var ops []op.Op
						if err := p.opManager.GetAllOpByTracking(plateTracking, &ops); err == nil && len(ops) != 0 {
							plateMap[plateTracking] = ops
						}
					} else if request.PlateType == "vp" {
						var vps []vp.Vp
						if err := p.vpManager.GetAllVpByTracking(plateTracking, &vps); err == nil && len(vps) != 0 {
							plateMap[plateTracking] = vps
						}
					} else if request.PlateType == "sp" {
						var sps []sp.Sp
						if err := p.spManager.GetAllSpByTracking(plateTracking, &sps); err == nil && len(sps) != 0 {
							plateMap[plateTracking] = sps
						}
					} else {
						mylog.Error.Println("Wrong plate type ", request.PlateType)
					}
					preTracking = plateTracking
				}
				break
			}
		}
	}

	var pcss []float64
	var ncs []float64
	pcss = nil
	ncs = nil

	var plates interface{}
	preTracking = ""

	for _, result := range results {
		hplc := new(Hplc)
		hplc.Date = time.Now().String()
		hplc.Tracking = trackNum
		hplc.Reactionph = request.Reactionph
		hplc.Temperature = request.Temperature
		hplc.Time = request.Time
		hplc.Name = request.Name
		hplc.Type = request.Type
		hplc.Dilution = request.Dilution
		hplc.Notes = request.Notes
		hplc.Creator = request.Creator
		plateTracking := result["plateTracking"]

		if preTracking != plateTracking {
			plates = plateMap[plateTracking]
			if len(pcss) != 0 && preTracking != "" {
				pcMap[preTracking] = pcss
				ncMap[preTracking] = ncs
				pcss = nil
				ncs = nil
			}
			preTracking = plateTracking
		}

		var selectedData string
		for key, value := range result {
			if key == request.SelectedColumn {
				selectedData = value
				break
			}
		}

		hplc.Od = result

		testResult, _ := strconv.ParseFloat(selectedData, 64)
		hplc.SelectedData = strconv.FormatFloat(testResult, 'f', 6, 64)

		switch ps := plates.(type) {
		case []op.Op:
			for _, p := range ps {
				if p.WellDefinition == result["wellDefinition"] {
					hplc.Colony = p.Colony
					hplc.PlateTracking = plateTracking
					hplc.LibraryName = p.Library.Name
					hplc.LibraryTracking = p.Library.Tracking
					hplc.GeneTracking = p.Library.Gene.Tracking
					hplc.WellDefinition = p.WellDefinition
					hplc.WellCode = p.WellCode
					hplc.ProjectTracking = p.Project.Tracking
					hplc.DataType = p.DataType
					hplc.PlateFormat = p.Format
					hplc.PlateType = p.Type
					if p.DataType == "Positive Control" {
						pcss = append(pcss, testResult)
					} else if p.DataType == "Negative Control" {
						ncs = append(ncs, testResult)
					}
				}
			}
		case []vp.Vp:
			for _, p := range ps {
				if p.WellDefinition == result["wellDefinition"] {
					hplc.Colony = p.Colony
					hplc.LibraryName = p.LibraryName
					hplc.LibraryTracking = p.LibraryTracking
					hplc.GeneTracking = p.Gene.Tracking
					hplc.WellDefinition = p.WellDefinition
					hplc.WellCode = p.WellCode
					hplc.ProjectTracking = p.Project.Tracking
					hplc.DataType = p.DataType
					hplc.PlateFormat = p.Format
					if p.DataType == "Positive Control" {
						pcss = append(pcss, testResult)
					} else if p.DataType == "Negative Control" {
						ncs = append(ncs, testResult)
					}
				}
			}
		case []sp.Sp:
			for _, p := range ps {
				if p.WellDefinition == result["wellDefinition"] {
					hplc.Colony = p.Colony
					hplc.LibraryName = p.LibraryName
					hplc.LibraryTracking = p.LibraryTracking
					hplc.GeneTracking = p.Gene.Tracking
					hplc.WellDefinition = p.WellDefinition
					hplc.WellCode = p.WellCode
					hplc.ProjectTracking = p.Project.Tracking
					hplc.DataType = p.DataType
					hplc.PlateFormat = p.Format
					if p.DataType == "Positive Control" {
						pcss = append(pcss, testResult)
					} else if p.DataType == "Negative Control" {
						ncs = append(ncs, testResult)
					}
				}
			}
		default:
			mylog.Error.Println("hplc get wrong type")
		}
		hplcs = append(hplcs, hplc)
	}

	if len(pcss) != 0 {
		pcMap[results[len(results)-1]["plateTracking"]] = pcss
	}
	if len(ncs) != 0 {
		ncMap[results[len(results)-1]["plateTracking"]] = ncs
	}

	// get ncAvg for each plate
	ncAvgMap := make(map[string]float64)
	for plate, nc := range ncMap {
		if len(nc) != 0 {
			ncAvgMap[plate] = calAvg(nc)
		}
	}

	PcResultMap := make(map[string]PcResult)

	for plate, pcs := range pcMap {
		var pcRes PcResult
		pcRes.pcAvg = calAvg(pcs)
		pcRes.pcStd = calStd(pcs)
		pcRes.pcCv = calCv(pcRes.pcAvg, pcRes.pcStd)
		PcResultMap[plate] = pcRes
	}

	for index, hplc := range hplcs {
		pcavg := PcResultMap[hplc.PlateTracking].pcAvg
		hplcs[index].Pcavg = strconv.FormatFloat(pcavg, 'f', 6, 64)
		pcstd := PcResultMap[hplc.PlateTracking].pcStd
		hplcs[index].Pcstd = strconv.FormatFloat(pcstd, 'f', 6, 64)
		pccv := PcResultMap[hplc.PlateTracking].pcCv
		hplcs[index].Pccv = strconv.FormatFloat(pccv, 'f', 6, 64)
		platePcs := pcMap[hplc.PlateTracking]
		ncAvg := ncAvgMap[hplc.PlateTracking]
		for index, platePc := range platePcs {
			platePcs[index] = platePc - ncAvg
		}

		pcavgn := calAvg(platePcs)
		pcstdn := calStd(platePcs)
		pccvn := calCv(pcavgn, pcstdn)
		hplcs[index].Pcavgn = strconv.FormatFloat(pcavgn, 'f', 6, 64)
		hplcs[index].Pcstdn = strconv.FormatFloat(pcstdn, 'f', 6, 64)
		hplcs[index].Pccvn = strconv.FormatFloat(pccvn, 'f', 6, 64)

		od, _ := strconv.ParseFloat(hplc.SelectedData, 64)
		hplcs[index].SelectedDatan = strconv.FormatFloat((od - ncAvg), 'f', 6, 64)

		if len(pcss) == 0 && ncAvg != 0 {
			pf := od / ncAvg
			hplcs[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
			pfn := od / ncAvg
			hplcs[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
		} else {
			pf := od / PcResultMap[hplc.PlateTracking].pcAvg
			hplcs[index].Pf = strconv.FormatFloat(pf, 'f', 6, 64)
			if calAvg(platePcs) != 0.0 {
				pfn := (od - ncAvg) / calAvg(platePcs)
				hplcs[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
			} else {
				pfn := 0.0
				hplcs[index].Pfn = strconv.FormatFloat(pfn, 'f', 6, 64)
			}
		}
	}
	return hplcs, nil
}

func calAvg(dataSet []float64) float64 {
	var sum float64
	sum = 0.0
	if len(dataSet) == 0 {
		return 0.0
	}
	for _, data := range dataSet {
		sum += data
	}
	dlength := len(dataSet)
	if dlength == 0 {
		return 0
	}
	avg := sum / (float64)(dlength)
	return avg
}

func calStd(dataSet []float64) float64 {
	avg := calAvg(dataSet)
	sum := 0.0
	for _, data := range dataSet {
		sum = sum + math.Abs(data-avg)*math.Abs(data-avg)
	}
	return math.Sqrt(sum / float64(len(dataSet)))
}

func calCv(avg float64, std float64) float64 {
	return std * 100 / avg
}
