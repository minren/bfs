package pv

import (
	"bitbucket.org/bfs/mongo"
	"bitbucket.org/bfs/mylog"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Pv struct {
	Id           bson.ObjectId `json:"id" bson:"_id"`
	GeneTracking string        `json:"geneTracking" bson:"geneTracking"`
	OpTracking   string        `json:"opTracking" bson:"opTracking"`
	OpWell       string        `json:"opWell" bson:"opWell"`
	Colony       string        `json:"colony" bson:"colony"`
	Type         string        `json:"type" bson:"type"`
	TestType     string        `json:"testType" bson:"testType"`
	Username     string        `json:"username" bson:"username"`
	Used         bool          `json:"used" bson:"used"`
}

// PvManager manages a list of pvs in memory.
type PvManager struct {
	g_session    *mgo.Session
	pvs          []*Pv
	mongoManager *mongo.MongoManager
}

// NewPvManager returns an empty PvManager.
func NewPvManager(session *mgo.Session) *PvManager {
	pvManager := &PvManager{}
	pvManager.g_session = session
	pvManager.mongoManager = mongo.NewMongoManager()
	return pvManager
}

// Save the given Pv in the PvManager.
func (p *PvManager) Save(pvs []Pv) error {
	mylog.Info.Println("Save: %s", pvs)
	// check if pv already exist

	var pvList []Pv
	if err := p.GetAllPv(&pvList); err != nil {
		mylog.Error.Println("Save: GetAllPv failed %v", err)
		return nil
	}

	var actPv []Pv
	var err error
	if len(pvList) == 0 {
		actPv = pvs
	} else {
		for _, pv := range pvs {
			dupPv := false
			for _, epv := range pvList {
/*				if epv.GeneTracking == pv.GeneTracking &&
					epv.OpTracking == pv.OpTracking &&
					epv.OpWell == pv.OpWell &&
					epv.Colony == pv.Colony &&
					epv.TestType == pv.TestType &&
					epv.Type == pv.Type &&
					epv.Username == pv.Username &&
					epv.Used == false {
						mylog.Warning.Println("Save: pv already exist")
						dupPv = true
						break
					}*/
				if epv.Colony == pv.Colony && epv.Type == pv.Type && epv.Username == pv.Username &&
					epv.Used == false {
					mylog.Warning.Println("Save: pv already exist")
					dupPv = true
					break
				}
			}
			if dupPv == false {
				actPv = append(actPv, pv)
			}
		}
	}

	for _, pv := range actPv {
		//update database
		err = p.CreatePv(&pv)
		if err == nil {
			// add to list in memory
			_ = append(p.pvs, &pv)
			mylog.Info.Println("Save: createPv succeed")
		} else {
			mylog.Error.Println("Save: createPv failed %v", err)
		}
	}

	return err
}

// All returns the list of all the Pvs in the PvManager.
func (p *PvManager) All(username string) ([]Pv, error) {
	var pvs []Pv
	var validPvs []Pv
	err := p.GetAllPv(&pvs)
	for _, pv := range pvs {
		if pv.Used == false && pv.Username == username {
			validPvs = append(validPvs, pv)
		}
	}
	return validPvs, err
}

func (p *PvManager) CreatePv(pv *Pv) error {
	col := "pv"
	pv.Id = bson.NewObjectId()
	// Write to mongo
	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.CreateObject(col, pv)
	if err != nil {
		mylog.Error.Println("ERR: Insert pv failed")
		return err
	}
	mylog.Info.Println("CreatePv, successfully insert to mongo")

	return nil
}

func (p *PvManager) RemovePv(pv *Pv) error {
	col := "pv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.RemoveObjectById(col, pv.Id)
	if err != nil {
		mylog.Error.Println("ERR: remove pv failed")
		return err
	}
	return nil
}

func (p *PvManager) UpdatePv(pv *Pv) error {
	col := "pv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	return p.mongoManager.UpdateObjectById(col, pv, pv.Id)
}

func (p *PvManager) GetAllPv(pvs *[]Pv) error {
	col := "pv"

	p.mongoManager.CopySession(p.g_session)
	defer p.mongoManager.CloseSession()

	err := p.mongoManager.GetAllObject(col, "_id", pvs)
	if err != nil {
		mylog.Error.Println("ERR: query pvs failed")
	}
	return err
}
